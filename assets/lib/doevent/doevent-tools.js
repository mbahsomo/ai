/**
Cara penggunaan 
masukkan dalam controller doeventTools
*/
String.prototype.replaceAll = function(s,r){return this.split(s).join(r)}
var doeventTools = angular.module('dTools', []);
doeventTools.$inject = ['$scope'];
doeventTools.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if(attrs.koma===undefined)
                attrs.koma = 0 ;

            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });
            ctrl.$parsers.unshift(function (viewValue) {                            
                elem.priceFormat({
                    prefix: '',
                    centsSeparator: '.',
                    thousandsSeparator: ',',
                    centsLimit: eval(attrs.koma)
                });
                return elem[0].value;
            });
        }
    };
}]);

doeventTools.factory('doeventTools', function($rootScope, $http, $filter) {
    var doeventToolsService = {};
    doeventToolsService.getObject = function(obj, valu, skey) {
        var hasil = {};
        angular.forEach(obj, function(value, key) {
            if (valu === value[skey]) {
                hasil = value;
                return false;
            }
        });
        //console.log(hasil);
        return hasil;
    };
    doeventToolsService.setObject= function(objAsal,objTujuan){};

    doeventToolsService.Translet = function(skey){
        try{
            if(bahasa[0].isi[0][skey]===undefined)
                return skey;
            else
                return bahasa[0].isi[0][skey];
        }catch(e){
            return "NA";
        }
    };

    doeventToolsService.DownloadFile = function(url){
        var link = document.createElement("a");
        link.download = name;
        link.href = url;
        link.click();
    };

    doeventToolsService.formatMoney = function(n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')', num = this.toFixed(Math.max(0, ~~n));

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    }

    doeventToolsService.getRp = function(newVal) {
        var valueRp="";
        var tmp ="";
        for(a=newVal.length-1; a >=0 ; a--){
            tmp = newVal.substring(a, a+1);
            if(tmp!=='.'){
                valueRp = tmp + valueRp;                
            }
        }
        return valueRp;
    };

    doeventToolsService.formatDate = function(v){
        var tgljam = v.split(' ');
        var tgl = tgljam [0];
        if( tgljam.length > 1 ){
            var jam = tgljam [1];
        }else{
            var jam = '';
        }
        var pecahTgl = tgl.split('-');
        return pecahTgl[2] + '-' + pecahTgl[1] + '-' + pecahTgl[0] + ' ' + jam;
    };

    doeventToolsService.formatRp = function(newVal) {
        return $filter('number')(newVal,2);
        /*var valueRp="";
        var tmp ="";
        var bil = 0 ;
        if(newVal!==undefined){
            for(a=newVal.length-1; a >=0 ; a--){
                tmp = newVal.substring(a, a+1);
                if(tmp!=='.'){
                    if (bil%3==0 && bil!==0){
                        tmp = tmp + '.';    
                    }
                    valueRp = tmp + valueRp;
                    bil++;
                }
            }
        }
        return valueRp;*/

       /* var valueRp="";
        var tmp ="";
        var bil = 0 ;
        var newValAll = newVal.split(",");
        var val = newValAll[0];
        var valkoma = newValAll[1];
        for(a=val.length-1; a >=0 ; a--){
            tmp = val.substring(a, a+1);
            if(tmp!=='.'){
                if (bil%3==0 && bil!==0){
                    tmp = tmp + '.';    
                }
                valueRp = tmp + valueRp;
                bil++;
            }
        }
        if(valkoma!==undefined){
            valueRp = valueRp + "," +  valkoma;
        }
        return valueRp;*/

    };

    doeventToolsService.setStatus = function(sts) {
        if (sts==='T'){
            return "glyphicon glyphicon-ok clstdcenter";
        }else{
            return "glyphicon glyphicon-remove clstdcenter";
        }
    };
    return doeventToolsService;
});