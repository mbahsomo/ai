function cssfile(url_css){
    return $("<link rel='stylesheet' type='text/css' href='"+url_css+"'>");
}
$("head").append(cssfile(base_url + 'assets/lib/bootstrap/css/bootstrap.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/font-awesome/css/font-awesome.min.css'));
//$("head").append(cssfile(base_url + 'assets/css/dashboard.css'));
$("head").append(cssfile(base_url + 'assets/css/t-table.css'));
$("head").append(cssfile(base_url + 'assets/css/style.css'));
$("head").append(cssfile(base_url + 'assets/css/bootstrap-theme.css'));

$("head").append(cssfile(base_url + 'assets/lib/loading/loading-bar.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/toaster/toaster.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/date-picker/date.css'));
$("head").append(cssfile(base_url + 'assets/lib/sweet-alert/sweet-alert.css'));
$("head").append(cssfile(base_url + 'assets/lib/ag-grid/angular-grid.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/ag-grid/theme-fresh.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/ag-grid/theme-dark.min.css'));
$script(
        [
            base_url + 'assets/lib/jquery/jquery-ui.min.js',
            base_url + 'assets/lib/bootstrap/js/bootstrap.min.js',
            base_url + 'assets/lib/angularjs/angular-route.min.js',
            base_url + 'assets/lib/angularjs/angular-animate.min.js',
            base_url + 'assets/lib/angularjs/angular-sanitize.js',
            base_url + 'assets/lib/loading/loading-bar.min.js',
            base_url + 'assets/lib/toaster/toaster.min.js',
            base_url + 'assets/js/price-format.js',
            base_url + 'assets/lib/doevent/doevent-print.js',
            base_url + 'assets/lib/doevent/doevent-tools.js',
            base_url + 'assets/lib/doevent/doevent-upload.js',
            base_url + 'assets/lib/doevent/doevent-input.js',
            base_url + 'assets/lib/angularjs/mask.min.js',
            base_url + 'assets/js/md5.min.js',
            base_url + 'assets/lib/auto-complite/ui-bootstrap-tpls-0.9.0.min.js',
            base_url + 'assets/lib/jquery/jquery.maskedinput.min.js',
            base_url + 'assets/lib/date-picker/date.min.js',
            base_url + 'assets/lib/date-picker/m-date.js',
            base_url + 'assets/lib/date-picker/locales/bootstrap-datepicker.id.js',            
            base_url + 'assets/lib/sweet-alert/sweet-alert.min.js',
            base_url + 'assets/lib/paging/paging.js',
            base_url + 'assets/lib/ag-grid/angular-grid.min.js'
        ],
        'app'
        );

var bahasa = [
    {"bahasa":"ina","isi":[]}
];



$script.ready('app', function() {

    var app = angular.module('DoEventApp', ['ngRoute','chieffancypants.loadingBar','toaster','dFile','ui.mask','ui.bootstrap','dDate','dTools','deFocus','ui.bootstrap','ngSanitize',"angularGrid","brantwills.paging"]);

    app.filter('dateEntry', function myDateFormat($filter) {
        return function(text) {
            if (text !== null) {
                var tempdate = new Date(text.replace(/-/g, "/"));
                return $filter('date')(tempdate, "dd-MM-yyyy HH:mm:ss");
            } else {
                return $filter('date')(new Date(), "dd-MM-yyyy HH:mm:ss");
            }
        };
    });
    app.provider('jsDeps', function() {
        this.$get = function(dependencies) {
            return {jsdeps: function($q, $rootScope) {
                    var deferred = $q.defer();
                    $script(dependencies, function() {
                        $rootScope.$apply(function() {
                            deferred.resolve();
                        });
                    });
                    return deferred.promise;
                }};
        };
    });


    app.config(function($routeProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, jsDepsProvider, $httpProvider)
    {
        app.controllerProvider = $controllerProvider;
        app.compileProvider = $compileProvider;
        app.routeProvider = $routeProvider;
        app.filterProvider = $filterProvider;
        app.provide = $provide;
        
        /*Master data*/
        $routeProvider.when('/', {
            templateUrl: site_url + 'awal/load_view',
            title: 'Awal',
            controller: 'AwalController',
            resolve: jsDepsProvider.$get([site_url + 'awal/load_controller'])
        }).when('/daftar_penyakit', {
            templateUrl: site_url + 'daftar_penyakit/load_view',
            controller: 'Daftar_penyakitController',
            resolve: jsDepsProvider.$get([site_url + 'daftar_penyakit/load_controller'])
        }).when('/gejala', {
            templateUrl: site_url + 'gejala/load_view',
            controller: 'GejalaController',
            resolve: jsDepsProvider.$get([site_url + 'gejala/load_controller'])
        }).when('/rule', {
            templateUrl: site_url + 'rule/load_view',
            controller: 'RuleController',
            resolve: jsDepsProvider.$get([site_url + 'rule/load_controller'])
        }).when('/konsultasi', {
            templateUrl: site_url + 'konsultasi/load_view',
            controller: 'KonsultasiController',
            resolve: jsDepsProvider.$get([site_url + 'konsultasi/load_controller'])
        });
        
        
    });


    /*app.controller('MenuCtrl', function($scope, $timeout, $location, $http, $filter) {
        $http.get( base_url + 'assets/app/lang/'+ setlang).success(function(response) {
            bahasa[0].isi.push(response);
        });    
    });*/

    app.controller('MenuCtrl', function($scope, $timeout, $location, $http, $filter) {
        var apple_selected, tree, treedata_avm, treedata_geography;
        treedata_avm = [{label:"Root"}];
        $scope.show_reload = false; 


    
    $scope.my_tree_handler = function(branch) {
        var _ref;
        //console.log(branch);
        if (branch.url !== undefined) {
            $location.path(branch.url);
        }
        $scope.output = "You selected: " + branch.label;
    };
    $scope.my_data = treedata_avm;
    $scope.my_tree = tree = {};
    /*$scope.loadMenu = function(){
        $http({
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url: site_url + 'menu/get_menu',
            method: "POST",
        }).success(function(data) {
            if (data !== undefined) {
                treedata_avm = data;
                $scope.my_data = [];
                $scope.doing_async = true;
                return $timeout(function() {
                    $scope.my_data = treedata_avm;
                    $scope.doing_async = false;
                    //return tree.expand_all();
                    //expand_all
                    //console.log(tree.get_children());
                }, 1000);
            }
        });
    };
    $scope.loadMenu();  

    $scope.keyUp = function(e) {
        var obj = e.currentTarget.activeElement.localName ;
        if(obj ==='body'){
            switch (e.which){
                case 37 :
                    $scope.my_tree.collapse_branch();
                    break;
                case 38 :
                    $scope.my_tree.select_prev_branch();
                    break;
                case 39 :
                    $scope.my_tree.expand_branch();
                    break;
                case 40 :
                    $scope.my_tree.select_next_branch();
                    break;
            }
        }
    };      
    
    /*$scope.reloadMenu = function() {
        $scope.my_data = [];
        $scope.doing_async = true;
        return $timeout(function() {
            if (Math.random() < 0.5) {
                $scope.my_data = treedata_avm;
            } else {
                $scope.my_data = treedata_geography;
            }
            $scope.doing_async = false;
            $scope.my_tree.expand_all();
        }, 1000);
        $scope.my_tree.expand_all();
    };
   */
    $http.get( base_url + 'assets/app/lang/'+ setlang).success(function(response) {
        bahasa[0].isi.push(response);
    });

    });

    angular.bootstrap(document.body, ['DoEventApp']);

});