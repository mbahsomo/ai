-- phpMyAdmin SQL Dump
-- version 4.0.10.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2015 at 06:09 AM
-- Server version: 5.1.41-3ubuntu12
-- PHP Version: 5.3.2-1ubuntu4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_spk`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `judul_berita` varchar(50) NOT NULL,
  `isi_berita` text NOT NULL,
  `gambar` text NOT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `id_user`, `judul_berita`, `isi_berita`, `gambar`) VALUES
(1, 1, 'Jangan gampang minum obat maag saat puasa', 'Jakarta ANTARA News  Orang yang tidak memiliki riwayat sakit maag tak disarankan mengonsumsi obat untuk mencegah penyakit ini selama berpuasa Spesialis penyakit dalam dari Rumah Sakit Ciptomangunkusumo Dr dr Ari Fahrial Syam mengatakan anggapan masyarakat bahwa untuk mencegah sakit maag saat berpuasa sebaiknya meminum obat maag tidaklah tepat Jelas mitos ini tidak tepat kata dia dalam keterangan tertulisnya hari ini Menurut Ari puasa akan membuat kondisi penderita sakit maag membaik apalagi bagi orang yang tidak memiliki riwayat sakit maag Oleh karena itu konsumsi obat maag bagi mereka yang tidak sakit maag tidak disarankan Justru orang yang mempunyai sakit maag sakit maagnya akan membaik saat berpuasa apalagi orang yang memang tidak mempunyai sakit maag tentu akan lebih sehat saat berpuasa dan tidak perlu obat untuk mencegah agar tidak sakit maag pungkas dia', '20110526031836sakit-perut.jpg'),
(2, 1, 'Tips Berpuasa Untuk Penderita Maag', 'Selama bulan suci Ramadan jutaan umat Muslim di seluruh dunia akan menjalankan ibadah puasa Secara umum umat Muslim yang mengalami gastritis atau peradangan pada lambung atau biasa disebut maag akan mempertanyakan bagaimana cara mereka bisa menunaikan ibadah dengan baik dan tidak memperburuk gejala penyakit yang sudah mereka alamiPada akhirnya kebanyakan dari mereka mengutamakan puasa tanpa mempertimbangkan efek terhadap kesehatannya Padahal bukan ini tujuan dari puasa Anda harus bisa menyeimbangkan antara puasa dan masalah kesehatan yang Anda deritaSebagai awalnya Anda harus memahami bagaimana riwayat dari penyakit ini Gastritis adalah infeksi atau inflamasi yang terjadi pada dinding lambung sementara maag adalah ketidakseimbangan asam lambung yang bisa mengganggu mekanisme perlindungan dari saluran cernaKedua penyakit diatas mempunyai penyebab yang sama yaitu infeksi bakteri yang dikenal dengan nama Helicobacter pylori H pylori yang mempengaruhi dinding saluran cerna terutama lambungPenggunaan obat anti nyeri seperti ibuprofen naproxen dan aspirin steroid dosis tinggi konsumsi alkohol dan merokok juga dapat menyebabkan gastritis dan maagPenderita gastritis tetap dapat menjalankan ibadah tanpa harus meningkatkan resiko komplikasi terhadap penyakit gastritis yang dideritanya asal mengikuti beberapa tips dibawah iniJangan tidur setelah habis sahur Tidur sehabis makan akan menghasilkan asam lambung yang naik keatas refluks asamMenunda sahur tapi mempercepat waktu buka puasaJangan makan berlebihan saat berbuka puasa karena akan memicu refluks asam dan gejala gastritis lainnyaPenderita asam lambung dan gastritis harus berbuka dengan secukupnya Makan dengan porsi yang kecil tetapi sering Sebagai contoh Anda bisa memulai berbuka puasa dengan kurma dan diikuti dengan makanan yang ringan sebelum mengambil jeda untuk sholat Mahgrib Setelah itu Anda bisa menikmati menu utama sebelum melaksanakan doa terawehBerhenti makan 34 jam sebelum Anda pergi tidur Oleh karena itu hindari konsumsi makanan kecil atau kudapan manis setelah selesai tarawehKurangi konsumsi makanan yang pedas panas dan makanan yang mengandung asam sehingga mengurangi resiko iritasi saluran cernaHindari konsumsi minuman yang mengandung kafein seperti kopi karena minuman ini bersifat diuretik yang akan menstimulasi tubuh mengeluarkan cairan melalui air kemih Hal ini bisa menyebabkan dehidrasi Hal yang sama juga terjadi pada minuman manisSaat sahur dan  berbuka lengkapi makanan Anda dengan makanan yang kaya nutrisi seperti karbohidrat protein lemak vitamin serat dan lain sebagainyaTetap cukupi kebutuhan cairan tubuh yaitu 25 liter perhariSiapkan obatobat yang memang Anda butuhkan untuk mengatasi gejala gastritis yang Anda alami misalnya saja antasida proton pump inhibitor H2 antagonis Tetapi Anda  memerlukan pengawasan dokter dalam mengkonsumsi obat iniHindari makanan yang berlemak dan buahbuahan yang mengandung asam misalnya buah citrus seperti lemon jeruk nipis anggur dan jeruk dan juga tomat karena tomat merupakan buah yang mengandung banyak asamMakanan ekstra pedas makanan kaleng terutama berbasis tomat juga harus Anda hindariJangan merokok Rokok dapat menimbulkan gejala gastritis dan maagJenis makanan yang harus Anda makanKonsumsi karbohidrat yang lambat cerna sehingga makanan tersebut akan dicerna dalam waktu lebih lama Anda akan lapar lebih lama sehingga Anda merasa lebih berenergi sepanjang hariKurma merupakan makanan yang sempurna karena mengandung gula serat karbohidrat potasium dan magnesiumAlmond merupakan jenis makanan yang kaya akan protein dan seratPisang merupakan buah yang mengandung karbohidrat potasium dan magnesiumPilihlah makanan yang di panggang dibanding makanan yang digoreng terlebih jika makanan tersebut berlemakJangan lupa mengkonsumsi obat yang Anda butuhkan saat sahur atau berbuka', 'tips-berpuasa-untuk-penderita-gastritis_670x310.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gejala`
--

CREATE TABLE IF NOT EXISTS `gejala` (
  `g_id` int(11) NOT NULL AUTO_INCREMENT,
  `g_name` varchar(45) NOT NULL,
  `g_pertanyaan` varchar(150) NOT NULL,
  `g_kepercayaan` double DEFAULT NULL,
  `user_entry` varchar(50) DEFAULT NULL,
  `date_entry` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`g_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `gejala`
--

INSERT INTO `gejala` (`g_id`, `g_name`, `g_pertanyaan`, `g_kepercayaan`, `user_entry`, `date_entry`, `date_edit`) VALUES
(1, 'Nyeri pada daerah lambung', 'Apakah anda merasakan Nyeri pada daerah lambung', 0.9, NULL, '2015-08-09 08:03:40', NULL),
(2, 'Rasa penuh di daerah lambung', 'Apakah anda merasakan Rasa penuh di daerah lambung', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(3, 'Kembung pada lambung', 'Apakah anda merasakan Kembung pada lambung', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(4, 'Nafsu makan berkurang', 'Apakah anda merasakan Nafsu makan berkurang', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(5, 'Mual atau muntah', 'Apakah anda merasakan Mual atau muntah', 0.9, NULL, '2015-08-09 08:03:40', NULL),
(6, 'Nyeri pada perut', 'Apakah anda merasakan Nyeri pada perut', 0.9, NULL, '2015-08-09 08:03:40', NULL),
(7, 'Nyeri pada dada', 'Apakah anda merasakan Nyeri pada dada', 0.7, NULL, '2015-08-09 08:03:40', NULL),
(8, 'Sering sendawa', 'Apakah anda merasakan Sering sendawa', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(9, 'Diare', 'Apakah anda merasakan Diare', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(10, 'Turunnya berat badan', 'Apakah anda merasakan Turunnya berat badan', 0.7, NULL, '2015-08-09 08:03:40', NULL),
(11, 'Pendarahan', 'Apakah anda merasakan Pendarahan', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(12, 'Keluarnya BAB warna hitam pekat', 'Apakah anda merasakan Keluarnya BAB warna hitam pekat', 0.9, NULL, '2015-08-09 08:03:40', NULL),
(13, 'Keluhan pada esofasus', 'Apakah anda merasakan Keluhan pada esofasus', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(14, 'Rasa nyeri terbakar pada tulang dada', 'Apakah anda merasakan Rasa nyeri terbakar pada tulang dada', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(15, 'Mulut terasa asam dan pahit', 'Apakah anda merasakan Mulut terasa asam dan pahit', 0.7, NULL, '2015-08-09 08:03:40', NULL),
(16, 'Rasa ada makanana atau minuman balik ke mulut', 'Apakah anda merasakan Rasa ada makanana atau minuman balik ke mulut', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(17, 'Batuk menahun', 'Apakah anda merasakan Batuk menahun', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(18, 'Serak', 'Apakah anda merasakan Serak', 0.7, NULL, '2015-08-09 08:03:40', NULL),
(19, 'Sakit tenggorokan', 'Apakah anda merasakan Sakit tenggorokan', 0.7, NULL, '2015-08-09 08:03:40', NULL),
(20, 'Asma', 'Apakah anda merasakan Asma', 0.7, NULL, '2015-08-09 08:03:40', NULL),
(21, 'Demam ringan atau meriang', 'Apakah anda merasakan Demam ringan atau meriang', 0.9, NULL, '2015-08-09 08:03:40', NULL),
(22, 'Kejang perut', 'Apakah anda merasakan Kejang perut', 0.8, NULL, '2015-08-09 08:03:40', NULL),
(23, 'Nyeri pada uluh hati', 'Apakah anda merasakan Nyeri pada uluh hati', 0.9, NULL, '2015-08-09 08:03:40', NULL),
(24, 'Nyeri pada tukak duodenum', 'Apakah anda merasakan Nyeri pada tukak duodenum', 0.9, NULL, '2015-08-09 08:03:40', NULL),
(25, 'Sering dehidrasi', 'Apakah anda merasakan Sering dehidrasi', 0.7, NULL, '2015-08-09 08:03:40', NULL),
(26, 'Mulas', 'Apakah anda merasakan Mulas', 0.8, NULL, '2015-08-09 08:03:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kombinasi`
--

CREATE TABLE IF NOT EXISTS `kombinasi` (
  `id_kombinasi` int(11) NOT NULL AUTO_INCREMENT,
  `id_penyakit` int(11) NOT NULL,
  `id_gejala` int(11) NOT NULL,
  `user_entry` varchar(50) DEFAULT NULL,
  `date_entry` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kombinasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `kombinasi`
--

INSERT INTO `kombinasi` (`id_kombinasi`, `id_penyakit`, `id_gejala`, `user_entry`, `date_entry`, `date_edit`) VALUES
(1, 1, 1, NULL, '2015-08-09 08:01:59', NULL),
(2, 1, 2, NULL, '2015-08-09 08:01:59', NULL),
(3, 1, 3, NULL, '2015-08-09 08:01:59', NULL),
(4, 1, 4, NULL, '2015-08-09 08:01:59', NULL),
(5, 1, 5, NULL, '2015-08-09 08:01:59', NULL),
(6, 2, 3, NULL, '2015-08-09 08:01:59', NULL),
(7, 2, 4, NULL, '2015-08-09 08:01:59', NULL),
(8, 2, 5, NULL, '2015-08-09 08:01:59', NULL),
(9, 2, 6, NULL, '2015-08-09 08:01:59', NULL),
(10, 2, 7, NULL, '2015-08-09 08:01:59', NULL),
(11, 3, 4, NULL, '2015-08-09 08:01:59', NULL),
(12, 3, 5, NULL, '2015-08-09 08:01:59', NULL),
(13, 3, 6, NULL, '2015-08-09 08:01:59', NULL),
(14, 3, 10, NULL, '2015-08-09 08:01:59', NULL),
(15, 3, 11, NULL, '2015-08-09 08:01:59', NULL),
(16, 3, 12, NULL, '2015-08-09 08:01:59', NULL),
(17, 4, 13, NULL, '2015-08-09 08:01:59', NULL),
(18, 4, 14, NULL, '2015-08-09 08:01:59', NULL),
(19, 4, 15, NULL, '2015-08-09 08:01:59', NULL),
(20, 4, 16, NULL, '2015-08-09 08:01:59', NULL),
(21, 4, 17, NULL, '2015-08-09 08:01:59', NULL),
(22, 4, 18, NULL, '2015-08-09 08:01:59', NULL),
(23, 4, 19, NULL, '2015-08-09 08:01:59', NULL),
(24, 4, 20, NULL, '2015-08-09 08:01:59', NULL),
(25, 5, 5, NULL, '2015-08-09 08:01:59', NULL),
(26, 5, 9, NULL, '2015-08-09 08:01:59', NULL),
(27, 5, 21, NULL, '2015-08-09 08:01:59', NULL),
(28, 5, 22, NULL, '2015-08-09 08:01:59', NULL),
(29, 5, 25, NULL, '2015-08-09 08:01:59', NULL),
(30, 5, 26, NULL, '2015-08-09 08:01:59', NULL),
(31, 6, 3, NULL, '2015-08-09 08:01:59', NULL),
(32, 6, 4, NULL, '2015-08-09 08:01:59', NULL),
(33, 6, 5, NULL, '2015-08-09 08:01:59', NULL),
(34, 6, 6, NULL, '2015-08-09 08:01:59', NULL),
(35, 6, 22, NULL, '2015-08-09 08:01:59', NULL),
(36, 6, 26, NULL, '2015-08-09 08:01:59', NULL),
(39, 7, 4, NULL, '2015-08-09 08:01:59', NULL),
(40, 7, 23, NULL, '2015-08-09 08:01:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `konsultasi`
--

CREATE TABLE IF NOT EXISTS `konsultasi` (
  `k_id` int(11) NOT NULL AUTO_INCREMENT,
  `k_name` varchar(45) DEFAULT NULL,
  `k_kelamin` char(1) DEFAULT NULL,
  `k_umur` int(3) DEFAULT NULL,
  `date_edit` datetime DEFAULT NULL,
  `user_entry` varchar(45) DEFAULT NULL,
  `date_entry` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`k_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `konsultasi`
--

INSERT INTO `konsultasi` (`k_id`, `k_name`, `k_kelamin`, `k_umur`, `date_edit`, `user_entry`, `date_entry`) VALUES
(12, 'sugik', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(13, 'Sidoarjo', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(14, 'sugik', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(15, 'sugik', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(16, 'Sugik', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(17, 'Sugik', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(18, 'xx', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(19, 'sugik', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(20, 'Eko', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(21, 'Sugik', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(22, 'eko', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(23, 'Sidoarjo', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(24, 'eko', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(25, 'eko', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(26, 'edo', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(27, 'eko', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(28, 'abldul', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(29, 'ed', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(30, 'abdul', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(31, 'a', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(32, 'a', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(33, 'a', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(34, 'a', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(35, 'a', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(36, '1', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(37, 'a', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(38, 'a', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(39, 'a', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(40, 'a', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(41, 'Test', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(42, 'a', 'L', 10, '2015-08-28 20:25:41', NULL, '2015-08-28 13:25:41'),
(43, 'a', 'L', 10, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `konsultasi_gejala`
--

CREATE TABLE IF NOT EXISTS `konsultasi_gejala` (
  `kg_id` int(11) NOT NULL AUTO_INCREMENT,
  `k_id` int(11) DEFAULT NULL,
  `g_id` int(11) DEFAULT NULL,
  `kg_status` char(1) DEFAULT 'F',
  PRIMARY KEY (`kg_id`),
  KEY `fk_konsultasi_gejala_konsultasi_idx` (`k_id`),
  KEY `fk_konsultasi_gejala_gejala1_idx` (`g_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=108 ;

--
-- Dumping data for table `konsultasi_gejala`
--

INSERT INTO `konsultasi_gejala` (`kg_id`, `k_id`, `g_id`, `kg_status`) VALUES
(2, 12, 1, 'T'),
(3, 12, 15, 'T'),
(4, 12, 1, 'T'),
(5, 12, 15, 'T'),
(6, 12, 16, 'T'),
(7, 12, 17, 'T'),
(8, 13, 1, 'T'),
(9, 13, 2, 'T'),
(10, 13, 3, 'T'),
(11, 13, 1, 'T'),
(12, 14, 1, 'T'),
(13, 14, 6, 'T'),
(14, 14, 7, 'T'),
(15, 15, 1, 'T'),
(16, 15, 2, 'T'),
(17, 15, 4, 'T'),
(18, 15, 5, 'T'),
(19, 16, 1, 'T'),
(20, 16, 2, 'T'),
(21, 16, 4, 'T'),
(22, 17, 6, 'T'),
(23, 17, 7, 'T'),
(24, 17, 8, 'T'),
(25, 17, 9, 'T'),
(26, 18, 6, 'T'),
(27, 18, 8, 'T'),
(28, 18, 7, 'T'),
(29, 19, 1, 'T'),
(30, 19, 2, 'T'),
(31, 19, 3, 'T'),
(32, 20, 1, 'T'),
(33, 20, 2, 'T'),
(34, 20, 3, 'T'),
(35, 20, 4, 'T'),
(36, 20, 6, 'T'),
(37, 20, 7, 'T'),
(38, 20, 8, 'T'),
(39, 21, 1, 'T'),
(40, 21, 3, 'T'),
(41, 21, 4, 'T'),
(43, 21, 26, 'T'),
(45, 22, 1, 'T'),
(46, 22, 3, 'T'),
(47, 23, 1, 'T'),
(48, 23, 2, 'T'),
(49, 23, 3, 'T'),
(50, 24, 20, 'T'),
(51, 24, 17, 'T'),
(52, 25, 6, 'T'),
(53, 25, 23, 'T'),
(54, 25, 15, 'T'),
(55, 26, 1, 'T'),
(56, 27, 1, 'T'),
(57, 27, 9, 'T'),
(58, 27, 19, 'T'),
(59, 27, 24, 'T'),
(60, 28, 6, 'T'),
(61, 28, 13, 'T'),
(62, 28, 23, 'T'),
(63, 29, 1, 'T'),
(64, 29, 2, 'T'),
(65, 29, 3, 'T'),
(66, 30, 1, 'T'),
(67, 30, 4, 'T'),
(68, 30, 8, 'T'),
(69, 30, 10, 'T'),
(70, 30, 20, 'T'),
(71, 30, 25, 'T'),
(72, 31, 1, 'T'),
(73, 32, 1, 'T'),
(74, 32, 5, 'T'),
(75, 32, 14, 'T'),
(76, 33, 1, 'T'),
(77, 33, 4, 'T'),
(78, 33, 6, 'T'),
(79, 34, 1, 'T'),
(80, 34, 2, 'T'),
(81, 34, 3, 'T'),
(82, 35, 1, 'T'),
(83, 35, 2, 'T'),
(84, 35, 3, 'T'),
(85, 36, 1, 'T'),
(86, 36, 9, 'T'),
(87, 36, 22, 'T'),
(88, 37, 1, 'T'),
(89, 37, 3, 'T'),
(90, 37, 9, 'T'),
(91, 38, 1, 'T'),
(92, 38, 2, 'T'),
(93, 39, 3, 'T'),
(94, 39, 4, 'T'),
(95, 39, 7, 'T'),
(96, 40, 1, 'T'),
(97, 40, 3, 'T'),
(98, 40, 12, 'T'),
(99, 41, 1, 'T'),
(100, 41, 3, 'T'),
(101, 41, 24, 'T'),
(102, 42, 1, 'T'),
(103, 42, 2, 'T'),
(104, 42, 3, 'T'),
(105, 43, 1, 'T'),
(106, 43, 2, 'T'),
(107, 43, 3, 'T');

-- --------------------------------------------------------

--
-- Table structure for table `konsultasi_penyakit`
--

CREATE TABLE IF NOT EXISTS `konsultasi_penyakit` (
  `kp_id` int(11) NOT NULL AUTO_INCREMENT,
  `k_id` int(11) DEFAULT NULL,
  `p_id` int(11) DEFAULT NULL,
  `kp_nilai` float DEFAULT '0',
  `user_entry` varchar(50) DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  `date_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`kp_id`),
  KEY `fk_konsultasi_penyakit_konsultasi1_idx` (`k_id`),
  KEY `fk_konsultasi_penyakit_penyakit1_idx` (`p_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `konsultasi_penyakit`
--

INSERT INTO `konsultasi_penyakit` (`kp_id`, `k_id`, `p_id`, `kp_nilai`, `user_entry`, `date_entry`, `date_edit`) VALUES
(1, 41, 1, 0.08, NULL, NULL, NULL),
(2, 42, 1, 0.98, NULL, NULL, NULL),
(3, 43, 1, 0.98, NULL, '2015-09-09 06:09:11', '2015-09-09 06:09:11');

-- --------------------------------------------------------

--
-- Table structure for table `penyakit`
--

CREATE TABLE IF NOT EXISTS `penyakit` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(30) DEFAULT NULL,
  `p_keterangan` varchar(45) DEFAULT NULL,
  `p_saran` varchar(45) DEFAULT NULL,
  `date_edit` datetime DEFAULT NULL,
  `user_entry` varchar(45) DEFAULT NULL,
  `date_entry` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `penyakit`
--

INSERT INTO `penyakit` (`p_id`, `p_name`, `p_keterangan`, `p_saran`, `date_edit`, `user_entry`, `date_entry`) VALUES
(1, 'Gastritis', 'peradangan yang terjadi dilambung akibat meni', 'Gastritis yang tidak parah dapat diberikan ob', NULL, NULL, NULL),
(2, 'Dispepsia', ' nyeri atau rasa tidak nyaman pada perut bagi', 'Bila tidak ditemukan penyebabnya, dokter akan', NULL, NULL, NULL),
(3, 'Kanker Lambung (Gastric cancer', 'Kanker lambung atau dikenal sebagai gastric c', 'Bedah, Radio terapi, Kemoterapi, Terapi merah', NULL, NULL, NULL),
(4, 'Gastroesophageal Reflux Diseas', 'GERD adalah proses aliran balik/refluks yang ', 'Pengobatan GERD melibatkan berbagai penggunaa', NULL, NULL, NULL),
(5, 'Gastroenteritis', 'Penyakit ini sering disebut diare atau mencre', 'oralit formula baru, pemberian zinc selama 10', NULL, NULL, NULL),
(6, 'Gastroparesis', 'Gastroparesis adalah penyakit kelumpuhan lamb', 'Pengobatan gastroparesis tergantung pada ting', NULL, NULL, NULL),
(7, 'Tukak Lambung', 'Tukak Lambung atau disebut dengan Peptic ulce', 'Antibiotik. Tukak lambung yang disebabkan ole', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `penyakit1`
--

CREATE TABLE IF NOT EXISTS `penyakit1` (
  `id_penyakit` int(11) NOT NULL AUTO_INCREMENT,
  `nama_penyakit` text NOT NULL,
  PRIMARY KEY (`id_penyakit`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `penyakit1`
--

INSERT INTO `penyakit1` (`id_penyakit`, `nama_penyakit`) VALUES
(1, 'Gastritis'),
(2, 'Dispepsia'),
(3, 'Kanker Lambung (Gastric cancer)'),
(4, 'Gastroesophageal Reflux Disease (GERD)'),
(5, 'Gastroenteritis'),
(6, 'Gastroparesis'),
(7, 'Tukak Lambung');

-- --------------------------------------------------------

--
-- Table structure for table `rule`
--

CREATE TABLE IF NOT EXISTS `rule` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) DEFAULT NULL,
  `g_id` int(11) DEFAULT NULL,
  `r_nilai` int(11) DEFAULT NULL,
  `r_solusi` varchar(100) DEFAULT NULL,
  `user_entry` varchar(50) DEFAULT NULL,
  `date_entry` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`r_id`),
  KEY `fk_rule_penyakit1_idx` (`p_id`),
  KEY `fk_rule_gejala1_idx` (`g_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `rule`
--

INSERT INTO `rule` (`r_id`, `p_id`, `g_id`, `r_nilai`, `r_solusi`, `user_entry`, `date_entry`, `date_edit`) VALUES
(1, 1, 1, 1, NULL, NULL, '2015-08-12 19:29:30', NULL),
(2, 1, 2, 1, NULL, NULL, '2015-08-12 19:29:30', NULL),
(3, 1, 3, 1, NULL, NULL, '2015-08-12 20:37:05', NULL),
(4, 1, 4, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(5, 1, 5, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(6, 2, 3, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(7, 2, 4, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(8, 2, 5, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(9, 2, 6, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(10, 2, 7, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(11, 2, 8, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(12, 3, 4, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(13, 3, 5, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(14, 3, 6, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(15, 3, 10, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(16, 3, 11, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(17, 3, 12, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(18, 4, 13, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(19, 4, 14, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(20, 4, 15, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(21, 4, 16, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(22, 4, 17, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(23, 4, 18, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(24, 4, 19, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(25, 4, 20, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(26, 5, 5, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(27, 5, 9, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(28, 5, 21, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(29, 5, 22, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(30, 5, 25, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(32, 6, 4, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(33, 6, 5, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(34, 6, 6, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(35, 6, 22, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(36, 6, 26, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(38, 7, 4, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(39, 7, 23, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(40, 7, 24, 1, NULL, NULL, '2015-08-12 20:37:06', NULL),
(43, 3, 3, 1, '', 'admin@admin.com', '2015-08-26 12:13:49', '2015-08-26 19:13:49'),
(44, 4, 3, 1, '', 'admin@admin.com', '2015-08-26 12:14:08', '2015-08-26 19:14:08'),
(45, 5, 3, 1, '', 'admin@admin.com', '2015-08-26 12:14:22', '2015-08-26 19:14:22');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_email` varchar(50) NOT NULL,
  `u_fname` varchar(45) DEFAULT NULL,
  `u_lname` varchar(45) DEFAULT NULL,
  `u_password` varchar(255) DEFAULT NULL,
  `u_salt` varchar(32) DEFAULT NULL,
  `user_entry` varchar(50) DEFAULT NULL,
  `date_entry` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`u_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_email`, `u_fname`, `u_lname`, `u_password`, `u_salt`, `user_entry`, `date_entry`, `date_edit`) VALUES
('admin@admin.com', 'Sugik', 'Puja Kusuma', 'bcaff8022271f758437d94a3b3ccd244', '348VZJ7D8KYXJ2PJVQ', 'wulan@mpm.com', '2015-02-01 15:48:10', '2015-02-08 04:59:56'),
('dodik@mpm.com', 'Dodik', 'D', '2579dc0c63696858a1cf5f82c52e1562', 'QDJH6C5B8CNTRHYK', 'admin@admin.com', '2015-05-12 16:12:41', '2015-05-12 16:12:41'),
('riyanto@mpm.com', 'Riyanto', 'R', 'caa5ad214324652915a1992f5d696f79', '260Z', 'admin@admin.com', '2015-02-06 17:38:40', '2015-02-07 00:38:40'),
('rudi', 'R', 'D', 'd321df4134b59a9b857765cc5234b8f6', 'NRQKLYS', 'admin@admin.com', '2015-05-04 16:01:25', '2015-05-04 23:01:25'),
('tatik@mpm.com', 'Tatik', 'T', '89b753686d6548b39183a52dc6d8b15f', 'F29PX', 'admin@admin.com', '2015-02-06 17:38:05', '2015-02-07 00:38:05'),
('wawan@mpm.com', 'Wawan', 'W', '51c814e21b7b252a3b0a9beff93729a2', 'WZTK85', 'admin@admin.com', '2015-02-06 17:38:17', '2015-02-07 00:38:17'),
('wulan@mpm.com', 'Wulan', 'D', '2ae936f6725b2e7892ccde23028e301d', 'GQ4ZS', 'admin@admin.com', '2015-02-06 17:37:46', '2015-02-07 00:37:46'),
('yusni@mpm.com', 'Yusni', 'Y', '8b224b218f13cc9f09405c59413ae76e', 'X7CF8JJ7VNZFJM0Z4G5X3D', 'admin@admin.com', '2015-05-12 16:10:43', '2015-05-12 16:10:43');

--
-- Triggers `user`
--
DROP TRIGGER IF EXISTS `user_AINS`;
DELIMITER //
CREATE TRIGGER `user_AINS` AFTER INSERT ON `user`
 FOR EACH ROW begin
  INSERT INTO user_menu (u_email, mn_id, umn_status,user_entry) 
  select new.u_email, mn_id, 'T',user_entry FROM menu;
END
//
DELIMITER ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `konsultasi_gejala`
--
ALTER TABLE `konsultasi_gejala`
  ADD CONSTRAINT `fk_konsultasi_gejala_gejala1` FOREIGN KEY (`g_id`) REFERENCES `gejala` (`g_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_konsultasi_gejala_konsultasi` FOREIGN KEY (`k_id`) REFERENCES `konsultasi` (`k_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `konsultasi_penyakit`
--
ALTER TABLE `konsultasi_penyakit`
  ADD CONSTRAINT `fk_konsultasi_penyakit_konsultasi1` FOREIGN KEY (`k_id`) REFERENCES `konsultasi` (`k_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_konsultasi_penyakit_penyakit1` FOREIGN KEY (`p_id`) REFERENCES `penyakit` (`p_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rule`
--
ALTER TABLE `rule`
  ADD CONSTRAINT `fk_rule_gejala1` FOREIGN KEY (`g_id`) REFERENCES `gejala` (`g_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rule_penyakit1` FOREIGN KEY (`p_id`) REFERENCES `penyakit` (`p_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
