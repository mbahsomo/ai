<?php
if(preg_match("/^koneksi.php/",$_SERVER["PHP_SELF"])){
    header('Location: index.php');
    exit;
}

define("MAX_GEJALA","10");
define("HOSTNAME", "localhost");
define("USERNAME", "root");
define("PASSWORD", "java");
define("DATABASE", "db_spk");
define("DRIVER_SQL", "mysqli");
define("BASE_URL", "http://".$_SERVER['SERVER_NAME']."/");
define("BASE_DOMAIN","localhost/kontraktor/");
define("COOKIE_DOMAIN","t-doeventapp");
define("BATAS_REC", "50");
define("INDEX", "index.php");
define("DEMO",TRUE);
define("HABIS","2015-10-10");
date_default_timezone_set('Asia/Jakarta');
define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');

if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false
 || strpos($_SERVER['HTTP_USER_AGENT'], 'CriOS') !== false) {
    define("BROWSER", "Chrome");
}else{
	define("BROWSER", "Bukan");
}

?>
