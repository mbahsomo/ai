<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(strtotime( date('Y-m-d') ) > strtotime(HABIS) ){
    exit ('<div align="center">Waktu Aktif Habis <br>Silakan Menghubungi <a href="http://www.do-event.com">http://www.do-event.com</a><br>Email : <a href="mailto:mbahsomo@gmail.com?Subject=Aktifasi%20Aplikasi" target="_top"">mbahsomo@gmail.com</a></div>');
}
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */
        
class T_Controller extends CI_Controller {

    public $cid = "";
    public $fn = "";
    public $detail = true;
    public $toko = array();
    public $mobile = false;
    
    function __construct() {
        parent::__construct();
        /*$data['new_expiration'] = 60*60*24*1;
        $this->session->sess_expiration = $data['new_expiration'];*/
        $this->cid = $this->uri->rsegment(1);
        $this->fn = ($this->uri->rsegment(2)!='')?$this->uri->rsegment(2):'index';
        if(!$this->_check_akses()){
            $this->_loaderror();
            exit;
            //redirect('/main/login/', 'refresh');
            //$this->load->view('template/login_view');
            //show_error("<div class='alert alert-info'>Anda tidak mempunyai hak akses [$this->cid/$this->fn ] </div><br><a class='btn btn-danger' href='".site_url('main/login')."'>Silakan Login Lagi</a>" , 404 );
        }else{
            $tk = $this->session->userdata('toko');
            $this->toko = $tk[0];
        }
        $this->load->library('user_agent');        
        $this->mobile= $this->agent->is_mobile();
    }

    private function _loaderror(){
        if($this->detail){
            /*$this->load->library('Doevent');
            $doe = new Doevent();
            $doe->hashkey();
            $data['controller'] =  ucfirst($this->cid) .'Controller';
            $doe->load_view( 'template/error_controller', $data);
            $this->load->view('template/error_view');*/
            echo 'Tidak Punya Hak Akses';
        }else{
            redirect('/main/login/', 'refresh');
        }        
    }

    function index(){}

    public function access_rules() {}

    private function _check_akses(){
        $rule = $this->access_rules();
        $control = false;
        $expression = false;
        if(is_array($rule)){
            $bil=0;
            foreach ($rule as $key => $value) {
                $expression = $value['expression'];
                if($value[0]=='allow'){
                    for ($a = 0 ; $a < count($value['actions']); $a++){
                        if($this->fn==$value['actions'][$a]){
                            $control = $expression;
                            break;
                        }
                    }
                    if($control)
                        break;
                }else if($value[0]=='deny' && $control){
                    $control = true;
                }
            }
            if($control && $expression){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    public function load_template($view,$data_dtl=null, $data_master = array()){
        $data['content'] =  $this->load->view($view,$data_dtl,true) ;
		$data = array_merge($data,$data_master);
        $this->load->view('template/main_view', $data,false);        
    }

    public function get_max_page($ttl, $limit){
        $vmod = $ttl % $limit;
        return ($ttl-$vmod);
    }

    public function cetak(){
        $this->load->library('table');
        $this->table->set_caption($this->cid);
        $tmpl = array (
            'table_open' => '<table border="1" style="border: 1px solid black;border-collapse:collapse;font-family:sans-serif;
font-size:8pt;text-transform: capitalize;width:100%;">'
        );

        $this->table->set_template($tmpl);

        $head = array();
        $fields = array();
        foreach ($this->mdl->get_rule() as $val) {
            if (isset($val['grid'])) {
                if ($val['grid']){
                    $head[] = $this->mdl->get_label($val['field']) ;
                    $fields[] = $val['field'];
                }
            }else{
                $head[] = $this->mdl->get_label($val['field']);
                $fields[] = $val['field'];
            }
        }
        $this->table->set_heading($head);
        echo $this->table->generate( $this->mdl->get_all($fields) );
        exit();
    }

    public function export_xls(){
        $this->load->library('table');
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=export.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        $this->cetak();
        exit();
    }

    public function export_doc(){
        $this->load->library('table');
        header("Content-Type: application/msword");
        header("Content-Disposition: attachment; filename=export.doc");
        header("Pragma: no-cache");
        header("Expires: 0");
        $this->cetak();
        exit();
    }

    public function get_akses($control, $evt){
        switch ($evt) {
            case 'v':
                return substr( $this->session->userdata( $control) , 0, 1);
                break;
            case 'a':
                return substr( $this->session->userdata( $control) , 1, 1);
                break;
            case 'e':
                return substr( $this->session->userdata( $control) , 2, 1);
                break;
            case 'd':
                return substr( $this->session->userdata( $control) , 3, 1);
                break;
            case 'p':
                return substr( $this->session->userdata( $control) , 4, 1);
                break;
            default:
                return 'F';
                break;
        }        
    }

    public function set_mobile(){
        redirect('/mobile/main/', 'refresh');
    }
}

/* End of file tcontroller.php */
/* Location: .//home/alif/project/public_html/t-rokok/payroll/controllers/tcontroller.php */