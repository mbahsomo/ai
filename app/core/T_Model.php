<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed');
if(strtotime( date('Y-m-d') ) > strtotime(HABIS) ){
    exit ('<div align="center">Waktu Aktif Habis <br>Silakan Menghubungi <a href="http://www.do-event.com">http://www.do-event.com</a><br>Email : <a href="mailto:mbahsomo@gmail.com?Subject=Aktifasi%20Aplikasi" target="_top"">mbahsomo@gmail.com</a></div>');
}

/**
 * @author Bella anto, Sugik puja kusuma, Airlangga bayu seto
 * @email bella.anto@yahoo.com, mbahsomo@do-event.com, qu4ck@iso.web.id
 */

/**
 * Description of entity_model
 * @property CI_Loader $load
 * @property CI_Session $session
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property JsonCI $jsonci
 */

class T_Model extends CI_Model{

    private  $fields, $params, $table, $orderby
    , $start, $stop, $join, $rec_id, $numrows
    , $groupby, $cetak_query, $key_field, $like, $like_or
    , $where_in, $field_where_in
    , $or_where_in, $field_or_where_in
    , $where_not_in, $field_where_not_in
    , $or_where_not_in, $field__where_not_in;
    //, $fk_table, $fk_key_field, $fk_return_field;
    
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        $this->params = NULL;
        $this->fields = '*';
        $this->orderby = NULL;
        $this->start = 0;
        $this->stop = NULL;
        $this->join = NULL;
        $this->cetak_query = FALSE;
        $this->numrows = 0;
        $this->like = "";
        $this->where_in = "";
        $this->or_where_in = "";
        $this->where_not_in = "";
        $this->or_where_not_in = "";
        $this->field_where_in = "";
        $this->field_or_where_in = "";
        $this->field_where_not_in = "";
        $this->field_or_where_not_in = "";
        $this->db->cache_delete_all();
    }
     
    /*function T_Model() {
        $this->params = NULL;
        $this->fields = '*';
        $this->orderby = NULL;
        $this->start = 0;
        $this->stop = NULL;
        $this->join = NULL;
        $this->cetak_query = FALSE;
        $this->numrows = 0;
        $this->like = "";
        $this->where_in = "";
        $this->or_where_in = "";
        $this->where_not_in = "";
        $this->or_where_not_in = "";
        $this->field_where_in = "";
        $this->field_or_where_in = "";
        $this->field_where_not_in = "";
        $this->field_or_where_not_in = "";
        $this->db->cache_delete_all();
    }*/
    
    public function get_key_field() {
        return $this->key_field;
    }

    /**
     * Setting null parameter
     */
    function set_null(){
        $this->params   = NULL;
        $this->fields   = '*';
        $this->orderby  = NULL;
        $this->start    = 0;
        $this->stop     = NULL;
        $this->join     = NULL;
        $this->groupby  = NULL;
        $this->cetak_query = FALSE;
        $this->numrows = 0;
        $this->like = "";
        $this->where_in = "";
        $this->or_where_in = "";
        $this->where_not_in = "";
        $this->or_where_not_in = "";
        $this->field_where_in = "";
        $this->field_or_where_in = "";
        $this->field_where_not_in = "";
        $this->field_or_where_not_in = "";
        $this->db->cache_delete_all();
    }

    /**
     *
     * @param boolean $cetak_query true : Print Query or False not print query
     */
    public function set_cetak_query($cetak_query) {
        $this->cetak_query = $cetak_query;
    }

    /**
     *
     * @access  public
     * @param <type> $groupby
     */
    function set_groupby($groupby){
        $this->groupby = $groupby;
    }

    /**
     *
     * @param <type> $key_field 
     */
    public function set_key_field($key_field) {
        $this->key_field = $key_field;
    }

    /**
     *
     * @access  public
     * @param String $fields Field yang akan di kembalikan atau yang akan di isikan
     */
    public function set_fields($fields) {
        if(is_array($this->fields)){
            $this->fields = array_merge($this->fields,$fields);
        }else{
            $this->fields = $fields;
        }
    }

    /**
    *
    * Untuk mengambil data fields
    */
    public function get_fields(){
        return $this->fields;
    }

    /**
     *
     * @param <type> $params
     */
    public function set_params($params) {
        if(is_array($this->params)){
            $this->params = array_merge($this->params,$params);
        }else{
            $this->params = $params;
        }
    }

    /**
     *
     * @param String $table table yang akan di select 
     */
    function set_table($table){
        $this->table = $table;
    }


    /**
     * 
     * Akan mengembalikan nama table
     */
    function get_table(){
        return $this->table;
    }

    /**
     *
     * @param <type> $orderby
     */
    function set_orderby($orderby){
        $this->orderby = $orderby;
    }

    /**
     *
     * @param <type> $start
     */
    function set_start($start){
        $this->start = $start;
    }

    /**
     *
     * @param <type> $stop
     */
    function set_stop($stop){
        $this->stop = $stop;
    }

    /**
     *
     * @param String $field_where_in
     * @param <type> $where_in 
     */
    public function set_where_in($field_where_in, $where_in) {
        $this->field_where_in = $field_where_in;
        $this->where_in = $where_in;
    }

    /**
     *
     * @param String $field_or_where_in
     * @param <type> $or_where_in 
     */
    public function set_or_where_in($field_or_where_in, $or_where_in) {
        $this->field_or_where_in = $field_or_where_in;
        $this->or_where_in = $or_where_in;
    }

    /**
     *
     * @param String $field_where_not_in
     * @param <type> $where_not_in 
     */
    public function set_where_not_in($field_where_not_in, $where_not_in) {
        $this->field_where_not_in = $field_where_not_in;
        $this->where_not_in = $where_not_in;
    }

    /**
     *
     * @param String $field_or_where_not_in
     * @param <type> $or_where_not_in
     */
    public function set_or_where_not_in($field_or_where_not_in, $or_where_not_in) {
        $this->field_or_where_not_in = $field_or_where_not_in;
        $this->or_where_not_in = $or_where_not_in;
    }

    /**
     *
     * @param <type> $like
     */
    public function set_like($like, $orr = false) {
        $this->like_or = $orr;
        if(is_array($this->like)){
            $this->like = array_merge($this->like,$like);
        }else{
            $this->like = $like;
        }
    }

    /**
     * Join antar table
     * 
     * @param array $join array(array('TABLE'=>nm_table,'FIELD'=>relasi,'JOIN'=>'left'))
     */
    function set_join($join){
        $this->join = $join;
    }

    /**
     *
     * @return <type>
     */
    function get_rec_id(){
        return $this->rec_id;
    }

    /**
     *
     * @return <type>
     */
    function get_numrows(){
        return $this->numrows;
    }

    /**
     * ambil data
     *
     * @return <type>
     */
    function get_data(){
        $this->db->select($this->fields);
        $this->db->from($this->table);
        #For Join table
        if ($this->join != NULL) {
            foreach($this->join as $jn){
                $this->db->join($jn['TABLE'],$jn['FIELD'],$jn['JOIN']);
            }
        }
        #For Where param
        if ($this->params != NULL) {
            $this->db->where($this->params);
        }

        #For Where like
        if ($this->like != NULL) {
            if(count($this->like)==1)
                $this->db->like($this->like);
            else{
                if ($this->like_or)
                    $this->db->or_like($this->like);
                else    
                    $this->db->like($this->like);
            }
        }

        #For Where in
        if ($this->where_in != NULL) {
            $this->db->where_in($this->field_where_in, $this->where_in );
        }

        #For or_where_in
        if ($this->or_where_in != NULL) {
            $this->db->or_where_in($this->field_or_where_in, $this->or_where_in );
        }

        #For where_not_in
        if ($this->where_not_in != NULL) {
            $this->db->where_not_in($this->field_where_not_in, $this->where_not_in );
        }

        #For or_where_not_in
        if ($this->or_where_not_in != NULL) {
            $this->db->or_where_not_in($this->field_or_where_not_in, $this->or_where_not_in );
        }

        #For Order By
        if ($this->orderby != NULL) {
            $this->db->order_by($this->orderby);
        }
        
        #For limit
        if ($this->stop != NULL && $this->start >= 0 ) {
            $this->db->limit($this->stop,$this->start);
        }

        #group
        if ($this->groupby != NULL) {
            $this->db->group_by($this->groupby);
        }

        $query = $this->db->get();
        log_message('error', 'Custom Error Query Getdata = '. $this->db->last_query() );
        if($this->cetak_query)
            echo $this->db->last_query(). '<br>';

        $arrdt = $query->result_array();
        $this->numrows =  count($arrdt);
        
        /*if ($query->num_rows()>1){
            $arrdt = $query->result_array();
        $this->numrows =  count($arrdt);
        }else if ($query->num_rows()==1){
            $arrdt = $query->row();
            $this->numrows =  1;
        }else{
            $arrdt = null;    
            $this->numrows =  0;
        }*/
        
        return $arrdt;
    }

    /**
     * Proses simpan data
     *
     * @return boolean
     */
    function save_data(){
        $this->db->trans_begin();
        $str = $this->db->insert_string($this->table, $this->fields);
        $this->db->query($str);
        log_message('error', 'Custom Error Query Save = '. $this->db->last_query() );
        try{
            $this->rec_id = $this->db->insert_id();
        } catch (Exception $e) {}
        if ($this->db->trans_status() === FALSE) {            
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            //Kosongkan hkey
            //$this->session->unset_userdata('hkey');
            //cetak query 
            return TRUE;
        }
    }

    /**
     * Proses udpate data
     *
     * @return <type> boolean
     */
    function update_data(){
        $this->db->trans_begin();
        $str = $this->db->update_string($this->table, $this->fields,$this->params);
        $this->db->query($str);
        log_message('error', 'Custom Error Query Update = '. $this->db->last_query() );
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            #cetak query
            if($this->cetak_query)
                echo $this->db->last_query(). '<br>';
            return FALSE;
        } else {
            $this->db->trans_commit();
            //$this->session->unset_userdata('hkey');
            #cetak query
            if($this->cetak_query)
                echo $this->db->last_query(). '<br>';
            return TRUE;
        }
    }

    /**
     * Proses delete data
     *
     * @return boolean  hasil
     */
    function delete_data(){
        $this->db->trans_begin();
        #For Where param
        if ($this->params != NULL) {
            $this->db->where($this->params);
        }

        #For Where like
        if ($this->like != NULL) {
            if(count($this->like)==1)
                $this->db->like($this->like);
            else
                $this->db->or_like($this->like);
        }

        #For Where in
        if ($this->where_in != NULL) {
            $this->db->where_in($this->field_where_in, $this->where_in );
        }

        #For or_where_in
        if ($this->or_where_in != NULL) {
            $this->db->or_where_in($this->field_or_where_in, $this->or_where_in );
        }

        #For where_not_in
        if ($this->where_not_in != NULL) {
            $this->db->where_not_in($this->field_where_not_in, $this->where_not_in );
        }

        #For or_where_not_in
        if ($this->or_where_not_in != NULL) {
            $this->db->or_where_not_in($this->field_or_where_not_in, $this->or_where_not_in );
        }

        $this->db->delete($this->table);        

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            #cetak query
            if($this->cetak_query)
                echo 'Gagal Delete = ' . $this->db->last_query(). '<br>';
            log_message('error', 'Custom Error Query Delete Gagal = '. $this->db->last_query() );
            return FALSE;
        } else {
            $this->db->trans_commit();
            //$this->session->unset_userdata('hkey');
            #cetak query
            if($this->cetak_query)
                echo $this->db->last_query(). '<br>';
            log_message('error', 'Custom Error Query Delete Sukses = '. $this->db->last_query() );
            return TRUE;
        }
    }

    /**
     *
     * @return Array 
     */
    public function get_tot_rows() {
        $this->db->select("count(". $this->table . '.' . $this->key_field .") as total");
        $this->db->from($this->table);
        #For Join table
        if ($this->join != NULL) {
            foreach($this->join as $jn){
                $this->db->join($jn['TABLE'],$jn['FIELD'],$jn['JOIN']);
            }
        }
        #For Where
        if ($this->params != NULL) {
            $this->db->where($this->params);
        }

        #For Where like
        if ($this->like != NULL) {
            if(count($this->like)==1)
                $this->db->like($this->like);
            else
                $this->db->or_like($this->like);
        }

        #For Where in
        if ($this->where_in != NULL) {
            $this->db->where_in($this->field_where_in, $this->where_in );
        }

        #For or_where_in
        if ($this->or_where_in != NULL) {
            $this->db->or_where_in($this->field_or_where_in, $this->or_where_in );
        }

        #For where_not_in
        if ($this->where_not_in != NULL) {
            $this->db->where_not_in($this->field_where_not_in, $this->where_not_in );
        }

        #For or_where_not_in
        if ($this->or_where_not_in != NULL) {
            $this->db->or_where_not_in($this->field_or_where_not_in, $this->or_where_not_in );
        }

        #group
        if ($this->groupby != NULL) {
            $this->db->group_by($this->groupby);
        }
        $this->db->limit(1,0);

        $query = $this->db->get();
        log_message('error', 'Custom Error Query Get Total Rows = '. $this->db->last_query() );
        if($this->cetak_query)
            echo $this->db->last_query(). '<br>';

        $arrdt = $query->result_array();
        return $arrdt[0]['total'];
    }


    /**
     * Cara Penggunaan :
     * 
     * $arr = array();
     * forearch ($item as $row){
     *  $row['images'] = $this->model->lookup_data('KD_ITEM = 1','tbl_member_item_images','MB_ITEM_ID, MB_ITEM_IMAGES');
     *  $arr[] = $row;
     * }
     *
     * @param String $fk_table table yang akan di jadikan relasi
     * @param String $kondisi key = kondisi
     * @param Array $fk_return_field field yang akan di kembalikan 
     * @return Array
     */
    public function lookup_data($kondisi, $fk_table, $fk_return_field='*'){
        $this->db->select($fk_return_field );
        $this->db->from($fk_table);

        $this->db->where($kondisi);
        $this->db->limit(1,0);
        $query = $this->db->get();
        log_message('error', 'Custom Error Query Lookup data = '. $this->db->last_query() );
        if($this->cetak_query)
            echo 'Isi Query = ' . $this->db->last_query() . '<br>';

        $arrdt = $query->row();
        return $arrdt;
    }

    public function exec_query($sql, $return = true){
        $query = $this->db->query($sql);
        if($this->cetak_query)
            echo $this->db->last_query(). '<br>';
        if ($return){
            return $query->result_array();
        }else{
            $query;
        }
    }

    public function get_field_array(){
        $arr = $this->get_rule();
        $find = array("record[","]");
        $arr_hasil = array();
        foreach ($arr as  $rows) {
            $fl = $rows['field'];
            $arr_hasil[]= str_replace($find, "", $fl);
        }
        return $arr_hasil;
    }

    public function get_rule($insert=true) {}

    public function get_label($kfield, $insert=true){
        $hasil = '';
        foreach ($this->get_rule($insert) as $val) {
            if($val['field']==$kfield){
                $hasil = $val['label'];
                break;
            }
        }
        return $hasil;
    }

    public function get_field_names($insert=true){
        $hasil = array();
        foreach ($this->get_rule($insert) as $val) {
            $hasil[$val['field']] =  isset ($val['value'])?$val['value']:'' ;
        }
        return $hasil;   
    }

    public function get_all($fields='*', $params=''){
        $this->set_null();
        //$this->set_cetak_query(true);
        $this->set_fields( $fields );
        if($params!=''){
            $this->set_params($params);
            $this->set_prams(array( $this->get_table() . '.toko_email' => $this->session->userdata('toko_email')));
        }
        return $this->get_data();
    }

}

/* End of file T_Model.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/core/T_Model.php */