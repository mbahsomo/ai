<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: awal.php
 */
class Awal extends T_Controller {

	var $stop = 0;
	function __construct(){
        parent::__construct();
		$this->stop = BATAS_REC;
	}

	public function access_rules() {
        return array(
            array('allow',
                'actions' => array('index','load_view','load_controller'),
                'expression' => $this->session->userdata('login')
            ),
            array('allow',
                'actions' => array('about'),
                'expression' => true
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function index() {
        
    }
    public function about()
    {
        $this->load->view($this->cid .'/about');
    }

    public function load_view(){
        $data ['evt_view'] = $this->get_akses( $this->cid, 'v');
        $data ['evt_add'] = $this->get_akses( $this->cid, 'a');
        $data ['evt_edit'] = $this->get_akses( $this->cid, 'e');
        $data ['evt_delete'] = $this->get_akses( $this->cid, 'd');
        $data ['evt_print'] = $this->get_akses( $this->cid, 'p');
        $this->load->view( $this->cid . '/view', $data);        
    }

    public function load_controller(){
    	$this->load->library('Doevent');
        $doe = new Doevent();
    	$doe->hashkey();
        $data['stop'] = $this->stop;
		$data['controller'] =  ucfirst($this->cid) .'Controller';
        $doe->load_view( $this->cid  .'/controller', $data);
    }    

}

/* End of file awal.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/sanmar/app/controllers/awal.php */