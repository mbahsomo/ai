<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam manage_user ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: manage_user.php
 */
class Manage_user extends T_Controller {

	var $stop = 0;
    var $menudata = array();

    function __construct() {
        parent::__construct();
        $this->load->model('user_model', 'mdl');
        $this->stop = BATAS_REC;
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('cetak','load_view','load_controller','index','insert_data','edit_data','delete_data','search','get_newcode','get_menu','edit_menu'),
                'expression' => ($this->get_akses( $this->cid, 'v')== 'T') ? true:false
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function load_view(){
        $data ['evt_view'] = $this->get_akses( $this->cid, 'v');
        $data ['evt_add'] = $this->get_akses( $this->cid, 'a');
        $data ['evt_edit'] = $this->get_akses( $this->cid, 'e');
        $data ['evt_delete'] = $this->get_akses( $this->cid, 'd');
        $data ['evt_print'] = $this->get_akses( $this->cid, 'p');
        $this->load->view( $this->cid . '/view', $data);        
    }

    public function load_controller(){
        $this->load->library('Doevent');
        $doe = new Doevent();
        $doe->hashkey();
        $data['stop'] = $this->stop;
        $data['controller'] =  ucfirst($this->cid) .'Controller';
        $doe->load_view( $this->cid  .'/controller', $data, '<span>');
    }

    public function insert_data() {
    	if($_POST['u_password']!=''){
            $_POST['u_salt'] = $this->mdl->get_salt();
            $_POST['u_password'] = $this->mdl->encrypted($_POST['u_password'],$_POST['u_salt']);
        }
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            //echo $error;
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->insert(), "kode" => $this->mdl->get_rec_id())));
        }
    }

    public function edit_data() {
    	if($_POST['u_password']!=''){
            $_POST['u_salt'] = $this->mdl->get_salt();
            $_POST['u_password'] = $this->mdl->encrypted($_POST['u_password'],$_POST['u_salt']);
        }
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->update($this->input->post('u_email', true)))));
        }
    }

    public function delete_data() {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                                array(
                                    'success' => $this->mdl->delete($this->input->post('u_email', true)),
                                    'max_page' => $this->get_max_page($this->mdl->get_tot_rows(), $this->stop)
                                )
        ));
    }

    public function search() {
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => true,
                'rec' => $this->mdl->search(
                        $this->input->post('field', true), $this->input->post('value', true), $this->input->post('stop', true), $this->input->post('limit', true)
                ),
                'max_page' => $this->mdl->get_tot_rows()
            )
        ));
    }

    private function _get_menu_anak($idup ,$html){
        $this->load->model('User_menu_model','mdl2');
        $data = $this->mdl2->search("mn_up;user.u_email;", $idup . ";" . $this->input->post('email',true).';',0,100);
        $html = $html . " &#8594; " ;
        foreach ($data as $rows) {
            $rows['mn_name'] = $html . $rows['mn_name'];
            $this->menudata[] = $rows;
            if($rows['mn_type']==='F'){
                $this->_get_menu_anak($rows['mn_id'],$html);
            }
        }
    }


    public function get_menu() {
        $this->load->model('User_menu_model','mdl1');
        $data = $this->mdl1->search("menu.mn_up;user.u_email;", "0;" . $this->input->post('email',true).';', 0, 1000);
        $html = "";
        $atas = 0 ;
        foreach ($data as $rows) {
            if( $rows['mn_up']==0){
                $html = "";
                $atas = 0;
            }
            if ($rows['mn_type']==='F'){
                $atas = 0 ;                    
            }else{
                $atas = 1;
            }            
            $this->menudata[] = $rows;
            if($rows['mn_type']==='F'){
                $this->_get_menu_anak($rows['mn_id'],$html);
            }
        }
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => true,
                'rec' => $this->menudata,
                'max_page' => $this->mdl1->get_tot_rows()
            )
        ));
    }

    public function edit_menu(){
        $this->load->model('User_menu_model','mdl1');
        $this->mdl1->set_null();
        $this->mdl1->set_fields(
            array(
                'umn_status' => $this->input->post('umn_status',true),
                'umn_add' => $this->input->post('umn_add',true),
                'umn_edit' => $this->input->post('umn_edit',true),
                'umn_delete' => $this->input->post('umn_delete',true),
                'umn_print' => $this->input->post('umn_print',true)
            )
        );
        $this->mdl1->set_params(array(
            'umn_id' => $this->input->post('umn_id',true)
        ));
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => $this->mdl1->update_data()
            )
        ));
    }
}

/* End of file ar.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/kpp-rungkut.lokal.com/app/controllers/ar.php */