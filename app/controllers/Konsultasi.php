<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create  : mbahsomo
 * Nama File    : Konsultasi.php
 */
class Konsultasi extends T_Controller {

    var $stop = 0;
    var $menudata = array();

    function __construct() {
        parent::__construct();
        $this->load->model('Konsultasi_model', 'mdl');
        $this->stop = BATAS_REC;
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('cetak','load_view','load_controller','index','insert_data','edit_data','delete_data','search','get_newcode','get_menu','edit_menu','export_xls','get_all'),
                'expression' => $this->session->userdata('login')
            ),
            array('allow',
                'actions' => array('manual','simpan_manual','manual_detail','simpan_detail','manual_detail_table','hapus_detail','hasil','konsulter','loadkonsultasi','get_total'),
                'expression' => true
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function load_view(){
        $this->load->view( $this->cid . '/view');
    }

    public function load_controller(){
        $this->load->helper('controller_helper');
        $this->load->library('Doevent');
        $doe = new Doevent();
        $doe->hashkey();
        $data['stop'] = $this->stop;
        $data['controller'] =  ucfirst($this->cid) .'Controller';
        $doe->load_view( $this->cid  .'/controller', $data, '<span><div>');
    }

    public function insert_data() {
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            //echo $error;
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->insert(), "kode" => $this->mdl->get_rec_id())));
        }
    }

    public function edit_data() {
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->update($this->input->post($this->mdl->get_key_field(), true)))));
        }
    }

    public function delete_data() {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                                array(
                                    'success' => $this->mdl->delete($this->input->post($this->mdl->get_key_field(), true)),
                                    'max_page' => $this->get_max_page($this->mdl->get_tot_rows(), $this->stop)
                                )
        ));
    }

    public function search() {
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => true,
                'rec' => $this->mdl->search(
                        $this->input->post('field', true), $this->input->post('value', true), $this->input->post('stop', true), $this->input->post('limit', true)
                ),
                'max_page' => $this->mdl->get_tot_rows()
            )
        ));
    }

    public function get_all() {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    array(
                        'rec' => $this->mdl->get_all()
                    )
        ));
    }

    public function manual()
    {
        $this->load->view($this->cid .'/manual');
    }
    public function konsulter()
    {
        $this->load->view($this->cid .'/konsulter');
    }

    public function manual_detail()
    {
        
        $this->load->view($this->cid .'/manual_detail');   
    }

    public function manual_detail_table()
    {
        $this->load->view($this->cid .'/manual_detail_table');   
    }

    public function hasil()
    {
        $this->load->helper('dmp_helper');
        $this->load->model('Rule_model','mdlrule');
        $this->load->model('Konsultasi_gejala_model');
        $this->Konsultasi_gejala_model->set_null();
        //$this->Konsultasi_gejala_model->set_cetak_query(true);
        $this->Konsultasi_gejala_model->set_fields('konsultasi_gejala.*, g_name, g_kepercayaan'); //, p_id
        $this->Konsultasi_gejala_model->set_join(array(
            /*array(
                'TABLE' => 'rule',
                'FIELD' => 'rule.g_id = konsultasi_gejala.g_id',
                'JOIN'  => 'inner'
            ),*/array(
                'TABLE' => 'gejala',
                'FIELD' => 'gejala.g_id = konsultasi_gejala.g_id',
                'JOIN'  => 'inner'
            )
        ));
        $this->Konsultasi_gejala_model->set_params(array('k_id' => $this->session->userdata('nokonsultasi')));
        $data['rec'] = $this->Konsultasi_gejala_model->get_data();
        $this->load->view($this->cid .'/hasil', $data);   
        //exit;
    }

    public function simpan_manual(){
        $sukses = false;
        $this->mdl->set_null();
        $this->mdl->set_fields(array(
            'k_name' => $this->input->post('nama',true),
            'k_kelamin'=> $this->input->post('kelamin',true),
            'k_umur'=> $this->input->post('umur',true),
        ));
        $sukses = $this->mdl->save_data();
        $session['nokonsultasi'] = $this->mdl->get_rec_id();
        $this->session->set_userdata($session); 
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    array(
                        'success' => $sukses,
                        'url' => site_url('konsultasi/manual_detail')
                    )
        ));
    }

    public function simpan_detail()
    {
        $this->load->model('Konsultasi_gejala_model');
        $this->Konsultasi_gejala_model->set_null();
        $this->Konsultasi_gejala_model->set_fields(array(
            'k_id' => $this->session->userdata('nokonsultasi'),
            'g_id' => $this->input->post('g_id',true),
            'kg_status' => 'T'
        ));
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    array(
                        'success' => $this->Konsultasi_gejala_model->save_data()
                    )
        ));

    }

    public function hapus_detail()
    {
        $this->load->model('Konsultasi_gejala_model');
        //Check apakah konsultasi sudah di isi atau belum
        $this->Konsultasi_gejala_model->set_null();
        $this->Konsultasi_gejala_model->set_params(array(
            'k_id' => $this->session->userdata('nokonsultasi'),
            'g_id' => $this->input->post('g_id',true)
        ));
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    array(
                        'success' => $this->Konsultasi_gejala_model->delete_data()
                    )
        ));
    }
    public function loadkonsultasi(){
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => true,
                'rec' => $this->mdl->cari(
                        $this->input->post('tgl1',true),$this->input->post('tgl2',true)
                )
            )
        ));
    }

    public function get_total()
    {
        $this->load->model('Konsultasi_gejala_model');
        $this->Konsultasi_gejala_model->set_null();
        $this->Konsultasi_gejala_model->set_fields('count(*) as total');
        $this->Konsultasi_gejala_model->set_params(array(
            'k_id' => $this->session->userdata('nokonsultasi')
        ));
        $data = $this->Konsultasi_gejala_model->get_data();
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => true,
                'total' => $data[0]['total']
                )
            )
        );
    }
}

/* End of file Konsultasi.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/c45/app/controllers/Konsultasi.php */