<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: main.php
 */
class Main extends T_Controller {

    public $detail = false;

	function __construct() {
        parent::__construct();
        $this->stop = BATAS_REC;
        if ($this->mobile){
            $this->set_mobile();
            exit;
        }
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('index','editpassword','updatepassword'),
                'expression' => $this->session->userdata('login')
            ),
            array('allow',
                'actions' => array('login','dologin','logout','register','get_cap','register_new','forgot_password','forgot_pass_new'),
                'expression' => true
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function index(){
        $this->load->library('Doevent');
        $doe = new Doevent();
        $doe->hashkey();
        $data['stop'] = $this->stop;
        $this->load_template('template/index_view',$data, array('path'=>true,'info'=>''));
    }

    public function login(){
        $this->load->view('template/login_view');
    }

    /*public function home(){
        $this->load->view('template/home_view');
    }*/

    public function dologin(){
        $success = false;
        $pesan = '';
        
        $this->load->model('User_model', 'mdl');
        $this->mdl->set_params( array('u_email'=>$this->input->post('username',true)) );
        $rec = $this->mdl->get_data();
        if( count($rec)>0){        	
        	if ($rec[0]['u_password']!== $this->mdl->encrypted($this->input->post('pass',true),$rec[0]['u_salt']) ){
        		$pesan = 'Password Salah';
        	}else{
                $success = true;
                if($this->input->post('ingat',true)){
                    $data['new_expiration'] = 0;//60*60*24*100;//30 days
                    $this->session->sess_expiration = $data['new_expiration'];
                }else{
                    $data['new_expiration'] = 60*60*24*30;
                    $this->session->sess_expiration = $data['new_expiration'];
                }              
                
                //Ambil toko
                $session['menu'] = $this->input->post('menu',true);
        		$session['login'] = 1;
                $session['name'] = $rec[0]['u_fname']  . ' ' . $rec[0]['u_lname'];                
                $session['user_name']= $this->input->post('username',true);
                $this->session->set_userdata($session);                
        	}
        	
        }else{
        	$pesan = 'username Tidak terdaftar';
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(
                json_encode(
                    array('success' => $success, 
                        "msg"=> $pesan ,
                        'url' => ($this->input->post('menu',true)==1)?site_url():site_url('kasir')
                    )
                )
            );
    }

    public function editpassword(){
        $this->load_template('user/editpassword_view');
    }

    public function updatepassword(){
        $this->load->model('User_model', 'mdl');
        $this->mdl->set_null();
        $this->mdl->set_table('tbl_user');
        $this->mdl->set_fields(array('u_password'=>$this->input->post('pass',true)));
        $this->mdl->set_params(array('u_name'=>$this->session->userdata('user_name')));
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('success' => $this->mdl->update_data() , "msg"=> "Update Password gagal" )));   
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }
    
}

/* End of file main.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/sanmar/app/controllers/main.php */