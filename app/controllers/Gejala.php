<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create  : mbahsomo
 * Nama File    : Gejala.php
 */
class Gejala extends T_Controller {

    var $stop = 0;
    var $menudata = array();

    function __construct() {
        parent::__construct();
        $this->load->model('Gejala_model', 'mdl');
        $this->stop = BATAS_REC;
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('cetak','load_view','load_controller','index','insert_data','edit_data','delete_data','search','get_newcode','get_menu','edit_menu','export_xls','get_all'),
                'expression' => $this->session->userdata('login')
            ),
            array('allow',
                'actions' => array('loadgejala'),
                'expression' => true
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function load_view(){
        $this->load->view( $this->cid . '/view');
    }

    public function load_controller(){
        $this->load->helper('controller_helper');
        $this->load->library('Doevent');
        $doe = new Doevent();
        $doe->hashkey();
        $data['stop'] = $this->stop;
        $data['controller'] =  ucfirst($this->cid) .'Controller';
        $doe->load_view( $this->cid  .'/controller', $data, '<span><div>');
    }

    public function insert_data() {
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            //echo $error;
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->insert(), "kode" => $this->mdl->get_rec_id())));
        }
    }

    public function edit_data() {
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->update($this->input->post($this->mdl->get_key_field(), true)))));
        }
    }

    public function delete_data() {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                                array(
                                    'success' => $this->mdl->delete($this->input->post($this->mdl->get_key_field(), true)),
                                    'max_page' => $this->get_max_page($this->mdl->get_tot_rows(), $this->stop)
                                )
        ));
    }

    public function search() {
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => true,
                'rec' => $this->mdl->search(
                        $this->input->post('field', true), $this->input->post('value', true), $this->input->post('stop', true), $this->input->post('limit', true)
                ),
                'max_page' => $this->mdl->get_tot_rows()
            )
        ));
    }

    public function get_all() {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    array(
                        'rec' => $this->mdl->get_all()
                    )
        ));
    }

    public function loadgejala(){
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => true,
                'rec' => $this->mdl->search(
                        'g_name', '', $this->input->post('stop', true), $this->input->post('limit', true)
                )
            )
        ));
    }
}

/* End of file Gejala.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/c45/app/controllers/Gejala.php */