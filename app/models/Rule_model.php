<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * Atribut Create   : mbahsomo
 * Nama File    : Rule_model.php
 */
class Rule_model extends T_Model {

    private $field = array();

    function __construct() {
        parent::__construct();
        $this->set_table('rule');
        $this->set_key_field( 'r_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        
        for ($i=0; $i < count($this->field) ; $i++) { 
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $fields['user_entry'] = $this->session->userdata('user_name');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function get_rule($insert=true) {
        $rl = array(
            array(
                'field' => 'p_id',
                'label' => 'Penyakit',
                'fieldLabel' => 'p_name',
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'g_id',
                'label' => 'Gejala',
                'fieldLabel' => 'g_name',
                'rules' => 'xss_clean|numeric|required'
            
            )
        );
        
        if(!$insert){
          return array_merge( 
            array(
                array(
                    'field' => 'r_id',
                    'label' => 'ID',
                    'rules' => 'xss_clean|numeric|required'
                )
            ),
            $rl
          );  
        }else{
            return $rl;
        }
    }
    
    public function search($field='umn_id', $value='%', $start=0, $stop=5){
        $this->set_null();
        //$this->set_cetak_query(true);
        $this->set_fields('rule.*, p_name, g_name');
        $this->set_join(array(
            array(
                'TABLE' => 'penyakit',
                'FIELD' => 'penyakit.p_id=' . $this->get_table() . '.p_id',
                'JOIN'  =>  'inner'
            ),array(
                'TABLE' => 'gejala',
                'FIELD' => 'gejala.g_id=' . $this->get_table() . '.g_id',
                'JOIN'  => 'inner'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        $fieldv = explode(";", $field);
        $valuev = explode(";", $value);
        if (count($valuev) > 0) {
            for ($a = 0; $a < count($valuev); $a++) {
                if ($valuev[$a] !== '') {
                    $this->set_params(array(
                         $fieldv[$a] => $valuev[$a]
                    ));
                }
            }
        }
        /*$this->set_orderby('menu.mn_urut , menu.mn_up, menu.mn_id');*/
        return $this->get_data();
    }
        
    public function get_penyakit($gid)
    {
        $this->set_null();
        $this->set_fields('p_id');
        $this->set_params(array('g_id'=>$gid));
        return $this->get_data();
    }
}

/* End of file Rule_model.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/sanmar/app/models/Rule_model.php */