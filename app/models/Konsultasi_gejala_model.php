<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: Konsultasi_gejala_model.php
 */
class Konsultasi_gejala_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::__construct();
        $this->set_table('konsultasi_gejala');
        $this->set_key_field( 'kg_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        
        for ($i=0; $i < count($this->field) ; $i++) { 
            if($this->field[$i]==='pm_status')
                $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetad_query(false);
        return $this->delete_data();
    }

    public function get_rule($insert=true) {
        $rl = array(
		    array(
		        'field' => 'k_id',
		        'label' => 'Konsuktasi',
		        'rules' => 'xss_clean|max_length[50]'
		    ),array(
		        'field' => 'g_id',
		        'label' => 'Gejala',
		        'rules' => 'xss_clean|max_length[50]'
		    ),array(
		        'field' => 'kg_status',
		        'label' => 'Status',
		        'rules' => 'xss_clean|max_length[1]'
		    )
        );
        
        if(!$insert){
          return array_merge( 
            array(
                array(
	                'field' => 'kg_id',
	                'label' => 'ID',
	                'rules' => 'xss_clean|numeric|required'
	            )
            ),
            $rl
          );  
        }else{
            return $rl;
        }
    }
    
    public function search($field='umn_id', $value='%', $start=0, $stop=5){
    	$this->set_null();
        //$this->set_cetak_query(true);
        $this->set_fields('Konsultasi_gejala.*, mn_name, mn_icon, mn_type, mn_url, mn_up, u_fname');
        $this->set_join(array(
        	array(
        		'TABLE'	=> 'konsultasi',
        		'FIELD'	=> 'konsultasi.k_id = Konsultasi_gejala.k_id',
        		'JOIN'	=> 'inner'   		
            ),array(
                'TABLE' => 'gejala',
                'FIELD' => 'gejala.g_id = Konsultasi_gejala.g_id',
                'JOIN'  => 'inner'
            )
    	));
        $this->set_start($start);
        $this->set_stop($stop);
        $fieldv = explode(";", $field);
        $valuev = explode(";", $value);
        if (count($valuev) > 0) {
            for ($a = 0; $a < count($valuev); $a++) {
                if ($valuev[$a] !== '') {
                    $this->set_params(array(
                         $fieldv[$a] => $valuev[$a]
                    ));
                }
            }
        }
        /*$this->set_orderby('menu.mn_urut , menu.mn_up, menu.mn_id');*/
        return $this->get_data();
    }

}

/* End of file Konsultasi_gejala_model.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/t-bts/app/models/Konsultasi_gejala_model.php */