<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * Atribut Create	: mbahsomo
 * Nama File	: Daftar_penyakit_model.php
 */
class Daftar_penyakit_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::__construct();
        $this->set_table('penyakit');
        $this->set_key_field( 'p_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        
        for ($i=0; $i < count($this->field) ; $i++) { 
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $fields['user_entry'] = $this->session->userdata('user_name');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function get_rule($insert = true) {
        $rl =  array(
            array(
                'field' => 'p_name',
                'label' => 'Nama Penyakit',
                'width' => 200,
                'rules' => 'xss_clean|max_length[25]|required'
            ),array(
                'field' => 'p_keterangan',
                'label' => 'Keterangan',
                'width' => 300,
                'rules' => 'xss_clean|max_length[200]'
            ),array(
                'field' => 'p_saran',
                'label' => 'Saran',
                'width' => 300,
                'rules' => 'xss_clean|max_length[200]'
            )
        );
        if (!$insert) {
            return array_merge(
                array(
                    array(
                        'field' => 'p_id',
                        'label' => 'ID',
                        'rules' => 'required|numeric|required'
                    )
                ), $rl
            );
        } else {
            return $rl;
        }
    }
    
    public function search($field='u_fname', $value='%', $start=0, $stop=5){
        $this->set_null();
        $this->set_fields( $this->get_table() . '.*');
        $this->set_start($start);
        $this->set_stop($stop);
        $fieldv = explode(";", $field);
        $valuev = explode(";", $value);
        if (count($valuev) > 0) {
            for ($a = 0; $a < count($valuev); $a++) {
                if ($valuev[$a] !== '') {
                    $this->set_like(array(
                        $this->get_table() . '.'.$fieldv[$a] => $valuev[$a]
                    ));
                }
            }
        }
        $this->set_orderby('date_edit desc');
        return $this->get_data();
    }
        

}

/* End of file Daftar_penyakit_model.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/sanmar/app/models/Daftar_penyakit_model.php */