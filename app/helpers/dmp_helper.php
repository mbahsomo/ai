<?php
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: dmp_helper.php
 */
if ( ! function_exists('set_penyakit'))
{
	function set_penyakit($sakit)
	{
		$s = "" ;
		foreach ($sakit as $key => $value) {
			//log_message('error', 'Error Penyakit = '. $value['p_id'] );
			if($s!==''){
				$s = $s . "," .$value['p_id'];
			}else{
				$s = $value['p_id'];
			}
		}
		log_message('error', 'Error Penyakit = '. $s );
		return $s;
	}

}

if ( ! function_exists('hasil_penyakit'))
{
	function hasil_penyakit($sakit)
	{
		$hasil = "" ;
		asort($sakit);
		//print_r ($sakit);	
		foreach ($sakit as $key => $value) {
			if($value[1]!==''){
				//print_r ($value);
				if ($hasil ==''){
					$hasil= $value[1];
				}
			}
		}
		return $hasil;
	}
}

if ( ! function_exists('kali_penyakit'))
{
	function kali_penyakit($sakit1, $sakit2)
	{
		$s = "" ;
		$data1 = explode(",", $sakit1);
		$data2 = explode(",", $sakit2);
		for ($i=0; $i < count($data1) ; $i++) { 
			for ($j=0; $j < count($data2) ; $j++) { 
				if($data1[$i] == $data2[$j]){
					if($s!==''){
						$s = $s . ',' . $data1[$i];
					}else{
						$s = $data1[$i];
					}
				}
			}	
		}
		//echo "data= ".$s;
		return $s;
	}
	
}

if ( ! function_exists('gabung_penyakit'))
{
	function gabung_penyakit($penyakit, $nilai)
	{
		
	}
}