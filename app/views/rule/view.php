<script type="text/ng-template" id="customTemplate.html">
  <a>
      <span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>
      <i>{{match.model.p_id}}</i>
      <i>{{match.model.p_name}}</i>
  </a>
</script>
<script type="text/ng-template" id="customTemplateGejala.html">
  <a>
      <span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>
      <i>{{match.model.g_id}}</i>
  </a>
</script>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Rule</h3>
    </div>
    <div class="panel-body">
        <div class="toolbarmain">
        <?php $this->load->view('template/toolbar'); ?>
        
        </div>
        <div class="gridStyleAg">
            <div style="height: 100%;" ag-grid="gridOptions" class="ag-fresh"></div>    
        </div>
        <div class="paging-fo">
            <paging
                class="small"
                page="currentPage" 
                page-size="pageSize" 
                total="ttlRec" 
                adjacent="2"
                dots=".."
                scroll-top="false" 
                hide-if-empty="true"
                ul-class="pagination"
                active-class="active"
                disabled-class="disabled"
                show-prev-next="true"
                paging-action="DoCtrlPagingAct('Paging Clicked', page, pageSize, total)">
            </paging> 
            <div class="total">Total Record : {{ttlRec}}</div>
        </div>        
    </div>
    <div class="panel-footer">
        <a type="button" href="#" class="btn btn-info">Home</a>
    </div>
</div>
<div class="modal fade bootstrap-dialog type-primary" id="window-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"> Add / Edit data</h4>
            </div>
            <div class="modal-body">
                <toaster-container toaster-options="{'time-out': 2000, 'close-button':true,'position-class':'toast-top-center'}"></toaster-container>
                <form class="form-horizontal" role="form" id="frmAturan" ng-submit="Save(dataRecSelect[0])">
                    <div class="form-group form-group-sm" >
                        <label for="p_id" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('p_id'); ?></label>
                        <div class="col-sm-8 input-group ">
                            <span class="input-group-addon ">{{dataPenyakit.p_id}}</span>
                            <input type="text" id="p_id" class="form-control input-sm" placeholder="Cari Penyakit"
                            ng-model="dataPenyakit"
                            typeahead="c as c.p_name for c in penyakit | filter:$viewValue | limitTo:10"
                            typeahead-min-length='1'
                            ng-change='onChangePart(dataPenyakit)'
                            typeahead-on-select='onSelectPart($item, $model, $label)'
                            typeahead-template-url="customTemplate.html" />                                   
                        </div>
                    </div>
                    <div class="form-group form-group-sm" >
                        <label for="g_id" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('g_id'); ?></label>
                        <div class="col-sm-8 input-group ">
                            <span class="input-group-addon ">{{dataGejala.g_id}}</span>
                            <input type="text" id="g_id" class="form-control input-sm" placeholder="Cari Gejala"
                            ng-model="dataGejala"
                            typeahead="c as c.g_name for c in gejala | filter:$viewValue | limitTo:10"
                            typeahead-min-length='1'
                            ng-change='onChangePart(dataGejala)'
                            typeahead-on-select='onSelectPart($item, $model, $label)'
                            typeahead-template-url="customTemplateGejala.html" />                                   
                        </div>
                    
                    
                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmAturan" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-floppy-disk"></span> {{setlang.Translet('save')}}</button>
                <button type="button" ng-click="CancelSave()" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-folder-open"></span> {{setlang.Translet('close')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Filter Form -->
<div class="modal fade bootstrap-dialog type-primary" id="window-filter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="glyphicon glyphicon-filter"></i> Filter</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group form-group-sm" ng-repeat="(key, value) in gridOptions.columnDefs" >
                        <label for="input{{value.field}}" class="col-sm-3 control-label">{{value.headerName}}</label>
                        <div class="col-sm-9">
                            <input de-focus type="text" class="form-control input-sm" id="input{{value.field}}" placeholder="{{value.headerName}}" ng-model="fields[value.field]">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" ng-click="FilterData()" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-filter"></i> {{setlang.Translet('filter')}}</button>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> {{setlang.Translet('close')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(".modal").draggable({
        handle: ".modal-header"
    });
</script>