<?php
$arrfields = array();
$arrfields = set_header_aggrid ($this->mdl->get_rule(true));
?>
<script type="text/javascript">
	angular.module('DoEventApp').controllerProvider.register('<?php echo $controller; ?>', function($scope, $http, $document, toaster, $filter, $sce, $compile, doeventTools) {
		$scope.pos = [];
		$scope.setlang = doeventTools;
		console.log($scope.setlang.Translet('edit'));
		$scope.dataRecSelect = [];
	    $scope.fields = [];
	    $scope.add = true;
	    $scope.ttlRec = 0;
	    $scope.filter = false;
	    $scope.penyakit = [];
	    $scope.dataPenyakit = [];
	    $scope.gejala = [];
	    $scope.dataGejala = [];
	    $scope.refresh = 1;
	    $scope.currentPage = 1;
	    $scope.pageSize = <?php echo BATAS_REC; ?>;
	    $scope.ttlRec = 0 ;

	    $scope.loadPenyakit = function(){
	    	$http({
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	            url: '<?php echo site_url( 'daftar_penyakit/get_all/' . $this->session->userdata('hashkey')) ; ?>',
	            method: "POST"
	        }).success(function(data) {
	            if (data !== undefined) {
	            	$scope.penyakit = data.rec;
	            	
	            }
	        });
	    };
	    $scope.loadPenyakit();

	    $scope.loadGejala = function(){
	    	$http({
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	            url: '<?php echo site_url( 'gejala/get_all/' . $this->session->userdata('hashkey')) ; ?>',
	            method: "POST"
	        }).success(function(data) {
	            if (data !== undefined) {
	            	$scope.gejala = data.rec;
	            }
	        });
	    };
	    $scope.loadGejala();

	    //setting autofocus
        angular.element('#window-modal').on('shown.bs.modal', function() {
            angular.element('input#per_code').focus();
        });
        
	    
	    $scope.LoadGrid = function() {
	    	var fieldcari = '';
            var fieldvalue = '';
            if($scope.filter){ 	
            	console.log($scope.fields);
	            if (angular.isObject($scope.gridOptions)) {
			    	angular.forEach($scope.gridOptions.columnDefs, function(value, key){
			    		if ($scope.fields[value.field] !== undefined && $scope.fields[value.field] !== ''){
			    			fieldcari += ((fieldcari!=='')?';':'')+ value.field ;
			    			fieldvalue += ((fieldvalue!=='')?';':'')+ $scope.fields[value.field] ;
			    		}
		            });
			    }
			}
			var vparams = {
                'field': fieldcari ,
                'value': fieldvalue ,
                'limit': $scope.pageSize,
                'stop' : eval ($scope.pageSize + '*('  + $scope.currentPage + "-1)")
            };
	        $http({
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	            url: '<?php echo site_url( $this->cid . '/search/' . $this->session->userdata('hashkey')) ; ?>',
	            method: "POST",
	            data: $.param(vparams)
	        }).success(function(data) {
	            if (data !== undefined) {
	                $scope.filter = false;
	                $scope.gridOptions.rowData = data.rec;
		        	$scope.gridOptions.api.onNewRows();
	            	$scope.ttlRec = data.max_page;
	            }
	        });
	    };
	    $scope.LoadGrid();

	    $scope.gridOptions = {
	        columnDefs: <?php echo json_encode($arrfields); ?>,
	        angularCompileRows: true,
	        rowData: null,
	        rowSelection: 'multiple',
	        enableColResize: true,
	        enableSorting: true,
	        enableFilter: true,
	        rowHeight: 22,
	        pinnedColumnCount: 1,
	        suppressRowClickSelection: true,
	    };

	    $scope.rowSelectedFunc = function(row){
	    	console.log(row);
	    }

	    $scope.DoCtrlPagingAct = function(text, page, pageSize, total){
	        $scope.currentPage = page;
	        $scope.LoadGrid();
	    };

	    $scope.FilterData = function() {
	    	$scope.filter = true;
	        $scope.LoadGrid();
	        angular.element('#window-filter').modal('hide');
	    };
	    
	    $scope.ResetFilter = function() {
	    	if (angular.isObject($scope.gridOptions)) {
		    	angular.forEach($scope.gridOptions.columnDefs, function(value, key){
		    		if ($scope.fields[value.field] !== undefined){
		    			$scope.fields[value.field] ='';
		    		}
	            });
		    }
	    };

	    $scope.ShowAdd = function() {
	    	$scope.add = true;
	    	$scope.gridOptions.rowData.push({
	  		});	  		
			$scope.gridOptions.api.onNewRows();
			$scope.gridOptions.api.selectIndex($scope.gridOptions.rowData.length -1,'single', true );
			$scope.gridOptions.api.ensureIndexVisible($scope.gridOptions.rowData.length -1);
			angular.copy($scope.gridOptions.selectedRows,$scope.dataRecSelect);

			$scope.dataPenyakit = {p_id:'', p_name :''};
  			$scope.dataGejala = {g_id:'', g_name :''};
			
			angular.element('#window-modal').modal(
	            {
	                top: '150px',
	                backdrop: false,
	                keyboard: false
	            }
	        );
	    };

	    $scope.ShowEdit = function() {
	        $scope.add = false;
	        if($scope.gridOptions.selectedRows.length===1){
	        	angular.copy($scope.gridOptions.selectedRows,$scope.dataRecSelect);
	        	$scope.dataPenyakit = {p_id:$scope.dataRecSelect[0].p_id,p_name:$scope.dataRecSelect[0].p_name};
	        	$scope.dataGejala = {g_id:$scope.dataRecSelect[0].g_id,g_name:$scope.dataRecSelect[0].g_name};

	        	console.log($scope.dataRecSelect[0]);
	        	angular.element('#window-modal').modal(
		            {
		                top: '250px',
		                backdrop: false,
		                keyboard: false
		            }
		        );
		    }else if($scope.gridOptions.selectedRows.length===0){
		    	swal({title: "Edit",text: "Pilih data dulu",timer: 2000,type: "error"});
		    }else {
		    	swal({title: "Edit",text: "Pilih 1 data saja",timer: 2000,type: "error"});
		    }
	    };

	    $scope.ShowDelete = function() {
	    	var dt = $scope.gridOptions.rowData;
	    	swal({   
		    		title: "Yakin Ingin Menghapus Data ..!",   
		    		text: "Data Yang sudah di hapus tidak akan bisa dikembalikan",   
		    		type: "warning",   
		    		showCancelButton: true,   
		    		confirmButtonColor: "#DD6B55",   
		    		confirmButtonText: "Ya, Hapus Data",   
		    		cancelButtonText: "Tidak, Batal Hapus",   
		    		closeOnConfirm: false,   
		    		closeOnCancel: true 
	    		},function(isConfirm){   
	    			if (isConfirm) {
	    				angular.forEach($scope.gridOptions.selectedRows, function(value, key){
	    					var vparams = {};
				        	vparams['<?php echo $this->mdl->get_key_field(); ?>'] = value.<?php echo $this->mdl->get_key_field(); ?>;
				        	$http({
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: '<?php echo site_url($this->cid . '/delete_data') ; ?>',
								method: "POST",
								data: $.param(vparams)
							}).success(function(data) {
								if (data !== undefined) {
									if (data.success === true) {

										dt.splice(dt.indexOf(value),1);
						            	swal({title: "Hapus..!",text: "Data Sudah terhapus",timer: 1000,type: "success"});
						            	$scope.gridOptions.rowData  = dt ;
						            	$scope.gridOptions.api.onNewRows();
									}
								}
							});
						});						
	    			}
	    		}
	    	);
	    };

	    $scope.CancelSave = function(){
	    	if ($scope.add){
	    		var dt = $scope.gridOptions.rowData;
	    		dt.splice(dt.length-1,1);
	            $scope.gridOptions.rowData  = dt ;
	            $scope.gridOptions.api.onNewRows();
	        }
	        angular.element('#window-modal').modal('hide');
	    }

	    $scope.ShowFilter = function() {
	        angular.element('#window-filter').modal(
	            {
	                top: '250px',
	                backdrop: false,
	                keyboard: false
	            }
	        );
	    };

	    $scope.Reload = function() {
	    	$scope.filter = false;
	        $scope.LoadGrid();
	    };

	    $scope.Save = function(item) {
	        var evt='';
			var vparams = {};
			if ($scope.add){
				evt = "insert_data";
			} else {
				evt = "edit_data";
			}
			
			angular.forEach(item, function(value, key){
	            if(key !== '$$hashKey')
	                vparams[key] = value;
	        });

	        vparams['p_id'] = $scope.dataPenyakit.p_id;
	        vparams['g_id'] = $scope.dataGejala.g_id;
	        
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url($this->cid ); ?>/' + evt,
				method: "POST",
				data: $.param(vparams)
			}).success(function(data) {
				if (data !== undefined) {
					/*item.per_code = $scope.dataPeriode.per_code;
					item.r_title = $scope.dataRekening.r_title;*/
					item.p_name = $scope.dataPenyakit.p_name;
					item.g_name = $scope.dataGejala.g_name;
					item.p_id = $scope.dataPenyakit.p_id;
					item.g_id = $scope.dataGejala.g_id;
					if (data.success === true) {
						if ($scope.add){
							item.<?php echo $this->mdl->get_key_field(); ?> = data.kode;
						}
						$scope.SetSuksesSave();
					}else{
						toaster.pop('error', "Proses Simpan", data.msg);
					}
				}
			});
	    };
		
		$scope.SetSuksesSave = function(){
			toaster.pop('success', "Proses Simpan", "Proses Simpan berhasil");
			var dt = $scope.gridOptions.rowData ;
			var idx = 0;
			angular.forEach(dt, function(rowItem) {
				if ($scope.gridOptions.selectedRows[0] === rowItem){
					idx =  dt.indexOf(rowItem);
					dt[idx] = $scope.dataRecSelect[0];
					return;
				}
			});
			$scope.gridOptions.rowData = dt ;
			$scope.gridOptions.api.onNewRows();
			$scope.gridOptions.api.selectIndex(idx,'single', true );

			if (confirm('Anda menambah data lagi ...!!! ')) {
		        $scope.ShowAdd();
		    }else{
		    	angular.element('#window-modal').modal('hide');
		    }
		}

		$scope.Print = function() {
	        print_preview('<?php echo site_url($this->cid .'/cetak'); ?>', '');
	    };

	    $scope.ExportXls = function(){
	    	doeventTools.DownloadFile('<?php echo site_url($this->cid . '/export_xls'); ?>');
	    }

	    $scope.set_status = function(val){
	    	if(val==='T'){
	    		return 'Aktif';
	    	}else{
	    		return 'Tdk Aktif';
	    	}
	    }
	});
</script>	