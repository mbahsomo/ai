<button type="button" class="btn btn-primary btn-sm" ng-click="ShowAdd()"><i class="glyphicon glyphicon-plus"></i> {{setlang.Translet('add')}}</button>
<button type="button" class="btn btn-success btn-sm" ng-click="ShowEdit()"><i class="glyphicon glyphicon-pencil"></i> {{setlang.Translet('edit')}}</button>
<button type="button" class="btn btn-danger btn-sm" ng-click="ShowDelete()"><i class="glyphicon glyphicon-remove"></i> {{setlang.Translet('delete')}}</button>
<!-- <button type="button" class="btn btn-default btn-sm" ng-click="Print()"><i class="glyphicon glyphicon-print"></i> {{setlang.Translet('print')}}</button> -->
<button type="button" class="btn btn-primary btn-sm" ng-click="Reload()"><i class="glyphicon glyphicon-refresh"></i> {{setlang.Translet('reload')}}</button>
<button type="button" class="btn btn-primary btn-sm" ng-click="ShowFilter()"><i class="glyphicon glyphicon-filter"></i> {{setlang.Translet('filter')}}</button>
<button type="button" class="btn btn-primary btn-sm" ng-click="ResetFilter()"><i class="glyphicon glyphicon-filter"></i> {{setlang.Translet('reset-filter')}}</button>
<!-- <button type="button" class="btn btn-primary btn-sm" ng-click="ExportXls()"><i class="fa fa-file-excel-o"></i> {{setlang.Translet('export-xls')}}</button> -->
