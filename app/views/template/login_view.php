<!DOCTYPE html>
<html lang="en" ng-app>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<?php echo base_url(); ?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                /*background-image:url('<?php echo base_url(); ?>assets/images/bts.jpg');*/
                background-repeat:no-repeat;
                background-size: 100%;
                
                background: rgba(212,228,239,1);
                background: -moz-linear-gradient(left, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
                background: -webkit-gradient(left top, right top, color-stop(0%, rgba(212,228,239,1)), color-stop(100%, rgba(134,174,204,1)));
                background: -webkit-linear-gradient(left, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
                background: -o-linear-gradient(left, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
                background: -ms-linear-gradient(left, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
                background: linear-gradient(to right, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d4e4ef', endColorstr='#86aecc', GradientType=1 );

            }

            .forgot{
                margin-top: 10px;
                margin-bottom: 10px;
            }

            .cls-logo{
                height: 100px;
            }

            .ref-btn{
                cursor: pointer;
            }

            .control-label{
                text-align: right;
            }

        </style>
      
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.png">
    </head>

    <body>
        <script type="text/javascript">
            function LoginCtrl($scope, $http, $sce){
                $scope.pesanerror = "";
                $scope.username = "";
                $scope.pass = "";
                $scope.ingat = true;
                $scope.menu = 1;
                $scope.showErrorMessage = function() {
                    return $sce.trustAsHtml($scope.pesanerror);
                };
                
                $scope.Login = function(){
                    var vparams = {
                        'username'  : $scope.username,
                        'pass'  : $.md5($scope.pass),
                        'ingat' : $scope.ingat,
                        'menu' : $scope.menu,
                        'cap' : $scope.cap
                    };
                    $http({
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '<?php echo site_url('main/dologin') ?>',
                        method: "POST",
                        data: $.param(vparams),
                    }).success(function(data) {
                        if (data != undefined) {
                            if (data.success == true) {
                                window.location.href = data.url;
                            } else {
                                $scope.pesanerror = data.msg;
                                $scope.errorshow = true;
                            }
                        }
                    });
                }
            }
        </script>
        <div class="container" ng-controller="LoginCtrl">
            <div class="row">
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                    <div class="panel panel-info">
                          <div class="panel-heading">
                                <h3 class="panel-title"></h3>
                          </div>
                          <div class="panel-body">
                                <!-- http://youtu.be/2NDCTH51m3g -->
                                <div class="alert alert-info">
                                    <strong></strong> 
                                    <div>
                                        <img class="cls-logo" src="<?php echo base_url('assets/images/logo.jpg');?>" class="img-responsive" alt="Image">
                                        
                                    </div>                                    
                                </div>
                                <div class="alert alert-info">
                                    <strong></strong>
                                    <h7> <p align="justify"> SELAMAT DATANG DI SISTEM PAKAR DIAGNOSA PENYAKIT LAMBUNG</p> </h7> 
                                    <div class="image_wrapper fl_img" align="center"><img src="<?php echo base_url('assets/images/lambung.jpg');?>" alt="image" width="164" height="182"/></div>
                                    <p align="justify">Penyakit pada lambung antara lain adalah sakit maag (gastritis), dispepsia dan Gastroesophageal Reflux Disease(GERD). Penyakit maag diakibatkan oleh asam lambung yang berlebihan, sehingga dinding lambung lama-lama tidak kuat menahan asam lambung tadi sehingga timbul luka. Dispepsiaorganik disebabkan adanya ketidakseimbangan faktor agresif dan defensif mukosa lambung/duodenum. Dispepsia fungsional disebabkan oleh berbagai penyebab yang bermacam-macam, tergantung tipenya. </p>
                                    <p align="justify">Untuk melakukan proses pendiagnosaan  penyakit caranya cukup mudah, yaitu pasien cukup menjawab pertanyaan-pertanyaan dari sistem berupa gejala  yang mungkin dirasakannya. Setelah itu sistem akan menampilkan hasil diagnosa penyakit pasien beserta perhitungannya.<br />
                                    </p>
                                    <h5>
                                    <p align="justify">Jenis gangguan lambung yang didiagnosa pada sistem ini terdiri atas 7  penyakit, yaitu  :</h5>
                                          <h6>
                                          <ol>
                                            <li><em> Gastritis</em></li>
                                              <li><em>Dispepsia</em></li>
                                              <li><em>Kanker Lambung </em></li>
                                              <li><em>GERD (Gastro-Esophageal Reflux Disease)</em></li>
                                              <li><em>Gastroenteritis</em></li>
                                              <li><em>Gastroparesis</em></li>
                                              <li><em>Tukak Lambung</em></li>
                                             
                                            </ol>
                                    </h6>
                                     
                                    <p> </p>
                                    <h5> METODE DEMPSTER SHAFER </h5>
                                                     <p align="justify">  Teori  Dempster-Shafer adalah  representasi,  kombinasi  dan  propogasi  ketidakpastian,  dimana  teori  ini  memiliki beberapa karakteristik yang secara instutitif sesuai dengan cara berfikir seorang pakar, namun dasar matematika yang kuat.Secara  umum  teori  Dempster-Shafer  ditulis  dalam suatu  interval:  [Belief,Plausibility].  Belief  (Bel) adalah ukuran kekuatan  evidence  dalam mendukung suatu himpunan  proposisi.  </p>
                                    </br>
                                    </br>
                                </div>
                          </div>
                    </div>
                </div>

                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="panel panel-primary" style="width:400px;">
                        <div class="panel-heading">
                                <h3 class="panel-title">Menu Utama</h3>
                        </div>
                        
                    <div class="sidebar_box">
                        <ul class="templatemo_list">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="<?php echo site_url('daftar_penyakit/detail'); ?>">Daftar Penyakit</a></li>
                            <li><a href="<?php echo site_url('konsultasi/manual'); ?>">Konsultasi</a></li>
                            <li><a href="<?php echo site_url('konsultasi/konsulter'); ?>">Laporan</a></li>
                            <li><a href="<?php echo site_url('awal/about'); ?>">About</a></li>
                            
                            <!-- <li><a href="?h=about">About</a></li> -->
                            
                       </ul>
                   </div>

                   <div align="center">
                                <img class="cls-logo" src="<?php echo base_url('assets/images/logo-login.jpg');?>" class="img-responsive" alt="Image">
                            </div>                            
                            <form ng-submit="Login()" class="form-signin" method="POST" ng-submit="Login()">
                                <div class="form-group form-group-sm" >
                                    <label for="po_code" class="col-sm-4 control-label">User Name</label>
                                    <div class="col-sm-8 ">
                                        <input required ng-keyup="$event.keyCode == 13 ? Login() : null" type="text" class="form-control input-sm" placeholder="Masukkan username" autofocus ng-model="username">
                                    </div>
                                </div>
                                <div class="form-group form-group-sm" >
                                    <label for="po_code" class="col-sm-4 control-label">Password</label>
                                    <div class="col-sm-8 ">
                                        <input required ng-keyup="$event.keyCode == 13 ? Login() : null" type="password" class="form-control input-sm" placeholder="Password" ng-model="pass">
                                    </div>
                                </div>

                                <div class="form-group form-group-sm" >
                                    <label for="po_code" class="col-sm-4 control-label">&nbsp;</label>
                                    <div class="col-sm-8 " style="text-align:left;" >
                                        <input type="checkbox" value="remember-me" name="ingat" ng-model="ingat"> Remember me
                                    </div>
                                </div>
                                
                                <div class="form-group form-group-sm" >
                                    <label for="po_code" class="col-sm-12 control-label" style="text-align:center;">
                                        <div ng-show="errorshow" class="alert alert-danger" role="alert">
                                            <button type="button" ng-click="errorshow=false" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <strong>Warning!</strong><div ng-bind-html='showErrorMessage()'></div>
                                        </div> 
                                    </label>
                                </div>

                                <button class="btn btn-primary" style="width:100%;" type="submit" ><span class="glyphicon glyphicon-user"></span> Sign in</button>
                                                                
                            </form>

                   </div>
                </div>
            </div>
                
        </div>





        <div class="span12">
            <footer>
            <p align="center">&copy; do-event.com</p>
            </footer>
        </div>

        <script src="<?php echo base_url('assets/lib/jquery/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/lib/angularjs/angular.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/js/md5.js'); ?>"></script>
    
    </body>
</html>
