<style type="text/css">
    /*.cls-logo{
        height: 60%;
        width: 20%;    
    }*/
    .navbar{
        background-color: lawngreen;
    }
</style>
<?php
//print_r ($this->session->sess_expiration);
?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <script type="text/javascript">
            var browser = '<?php echo BROWSER; ?>';
            var base_url = '<?php echo base_url(); ?>';
            var site_url = '<?php echo site_url(); ?>/';
            var setlang = 'ina.json';
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

        <title>System Pakar Penyakit Lambung</title>
        
    </head>
    <body>
        <nav class="navbar">
          <div class="container-fluid">
            <div id="navbar" class="navbar-collapse collapse">
              <img class="cls-logo" src="<?php echo base_url('assets/images/logo.jpg');?>" class="img-responsive" alt="Image">
            </div>
          </div>
        </nav>

    <div class="container-fluid" ng-controller="MenuCtrl">
        
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Sistem Pakar</h3>
                </div>
            <div class="col-sm-3 col-md-2 sidebar">
                
            <ul class="nav nav-sidebar">
                    <!-- <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li> -->
                    <li><a href="#">Home</a></li>
                    
                    <li><a href="#daftar_penyakit">Data Penyakit</a></li>
                    <li><a href="#gejala">Data Gejala</a></li>
                    <li><a href="#rule">Rule</a></li>
                    <!-- <li><a href="#konsultasi">Konsultasi</a></li> -->
                    <li><a href="<?php echo site_url('main/logout'); ?>">Log Out</a></li>
            </ul>
            </div>
            
            <div class="col-sm-2 col-md-10" style="margin-top:10px;">
                <!-- <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                </div> -->
                <ng-view></ng-view>
            </div>
        </div>
    </div>
                

        <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/angular.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/script.js"></script>
        <script src="<?php echo base_url(); ?>assets/app/app.js"></script>
        
    </body>
</html>