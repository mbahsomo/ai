
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- The above 2 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Note there is no responsive meta tag here -->

        <link rel="icon" href="favicon.ico">

        <title>Hitung</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/lib/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/angular.min.js"></script>
    
    </head>

    <body>

        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- The mobile navbar-toggle button can be safely removed since you do not need it in a non-responsive implementation -->
                    <a class="navbar-brand" href="#">Konsultasi</a>
                </div>
                <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
                <div id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>            
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <style type="text/css">
            .detail{
                margin-top: 60px;
            }
        </style>
        <script type="text/javascript">
            function ManualCtrl($scope, $http){
                $scope.datasimpan = {
                    nama:'',
                    kelamin : 'L',
                    umur : 10
                };
                $scope.Save = function(){
                    $http({
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '<?php echo site_url( $this->cid . '/simpan_manual/' . $this->session->userdata('hashkey')) ; ?>',
                        method: "POST",
                        data: $.param($scope.datasimpan)
                    }).success(function(data) {
                        if (data !== undefined) {
                            if(data.success==true)
                                window.location.href = data.url;
                        }
                    });
                }
            }
        </script>
        <div class="container detail" ng-app>        
            <div class="panel panel-success" ng-controller="ManualCtrl">
                <div class="panel-heading">
                    <h3 class="panel-title">Hitung</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" id="frmSimpan" ng-submit="Save()">
                        <div class="form-group form-group-sm" >
                            <label for="p_name" class="col-sm-3 control-label">Nama</label>
                            <div class="col-sm-3">
                                <input required de-focus type="text" class="form-control input-sm" ng-model="datasimpan.nama" placeholder="Masukkan Nama">
                            </div>
                        </div>
                        <div class="form-group form-group-sm" >
                            <label for="p_name" class="col-sm-3 control-label">Kelamin</label>
                            <div class="col-sm-3">
                                <select ng-model="datasimpan.kelamin" class="form-control input-sm" >
                                    <option>Pilih Kelamin</option>
                                    <option value="L">Laki - Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-group-sm" >
                            <label for="p_name" class="col-sm-3 control-label">Umur</label>
                            <div class="col-sm-1">
                                <input required de-focus type="text" class="form-control input-sm" ng-model="datasimpan.umur" placeholder="Umur">
                            </div>
                        </div>
                    </form>
                    <button type="submit" form="frmSimpan" class="btn btn-primary">Simpan</button>
                </div>
            </div>

        </div> <!-- /container -->
    
    </body>
</html>
