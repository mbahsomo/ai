<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- The above 2 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Note there is no responsive meta tag here -->

        <link rel="icon" href="favicon.ico">

        <title>Hitung</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/lib/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/angular.min.js"></script>
        
        <style type="text/css">
            .detail{
                margin-top: 60px;
            }
        </style>
    </head>

    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- The mobile navbar-toggle button can be safely removed since you do not need it in a non-responsive implementation -->
                    <a class="navbar-brand" href="#">Gejala</a>
                </div>
                <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
                <div id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>            
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <script type="text/javascript">
            function KasusCtrl($scope, $http){
                $scope.LoadPertanyaan = function(){
                    $scope.pertanyaan = [] ;
                    $http({
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '<?php echo site_url( 'gejala/loadgejala/' . $this->session->userdata('hashkey')) ; ?>',
                        method: "POST",
                        data: $.param({stop:5, limit :1000})
                    }).success(function(data) {
                        if (data !== undefined) {
                            if(data.success==true){
                                $scope.pertanyaan = data.rec;
                                console.log($scope.pertanyaan);
                            }
                        }
                    });
                }
                $scope.LoadPertanyaan();

                $scope.SimpanPertanyaan = function(item){
                    if($scope.maxtanya< <?php echo MAX_GEJALA; ?>){
                        var vurl = '<?php echo site_url( 'konsultasi/simpan_detail/' . $this->session->userdata('hashkey')) ; ?>';
                        if(item.pilih != 'T'){
                            vurl ='<?php echo site_url( 'konsultasi/hapus_detail/' . $this->session->userdata('hashkey')) ; ?>'
                        }
                        $scope.inc++;
                        $http({
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            url: vurl,
                            method: "POST",
                            data: $.param({g_id: item.g_id})
                        }).success(function(data) {
                            if (data !== undefined) {
                                if(data.success==true){
                                    console.log(data);
                                    $scope.LoadMaxPertanyaan();
                                    if($scope.maxtanya>= <?php echo MAX_GEJALA; ?>){
                                        window.location.href = '<?php echo site_url('konsultasi/hasil'); ?>';
                                    }
                                }
                            }
                        });
                    }else{
                        window.location.href = '<?php echo site_url('konsultasi/hasil'); ?>';
                    }
                }

                $scope.maxtanya = 0;
                $scope.LoadMaxPertanyaan = function(){
                    $http({
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '<?php echo site_url( 'konsultasi/get_total/' . $this->session->userdata('hashkey')) ; ?>',
                        method: "POST"
                    }).success(function(data) {
                        if (data !== undefined) {
                            if(data.success==true){
                                $scope.maxtanya = data.total;
                            }
                        }
                    });
                }

                $scope.LoadMaxPertanyaan();

            }
        </script>
        <div class="container detail" ng-app>        
            <div class="panel panel-success" ng-controller="KasusCtrl">
                <div class="panel-heading">
                    <h3 class="panel-title">Gejala Yang di rasakan</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        <strong>Max Pemilihan Gejala = <?php echo MAX_GEJALA; ?></strong> Jumlah yang sudah dipilih {{maxtanya}}
                    </div>

                    <div class="alert alert-success">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Pertanyaan</th>
                                        <th>Jawaban</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="items in pertanyaan">
                                        <td>{{items.g_id}}</td>
                                        <td>{{items.g_pertanyaan}}</td>
                                        <td>
                                            <form>
                                            <input ng-change="SimpanPertanyaan(items)" type="radio" ng-model="items.pilih" value="T" name="pilih">Iya &nbsp;
                                            <input ng-change="SimpanPertanyaan(items)" type="radio" ng-model="items.pilih" value="F" name="pilih">Tidak
                                            </form>
                                        </td>
                                        <td>{{items.g_kepercayaan}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <a type="button" class="btn btn-danger" href="<?php echo site_url('konsultasi/hasil'); ?>">Selesai</a>
                </div>
            </div>

        </div> <!-- /container -->
        
    </body>
</html>
