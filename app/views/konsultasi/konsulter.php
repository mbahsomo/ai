<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- The above 2 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Note there is no responsive meta tag here -->

        <link rel="icon" href="favicon.ico">

        <title>Laporan</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/lib/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/angular.min.js"></script>
        
        <style type="text/css">
            .detail{
                margin-top: 60px;
            }
        </style>
    </head>

    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- The mobile navbar-toggle button can be safely removed since you do not need it in a non-responsive implementation -->
                    <a class="navbar-brand" href="#">Laporan</a>
                </div>
                <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
                <div id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>            
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <script type="text/javascript">
            function KasusCtrl($scope, $http){
                $scope.loadKonsultasi = function(){
                    $scope.konsultasi = [] ;
                    $scope.tgl1 = '<?php echo date('m/d/Y'); ?>';
                    $scope.tgl2 = '<?php echo date('m/d/Y'); ?>';
                    $http({
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '<?php echo site_url( 'konsultasi/loadkonsultasi/' . $this->session->userdata('hashkey')) ; ?>',
                        method: "POST",
                        data: $.param({tgl1:'',tgl2 :''})
                    }).success(function(data) {
                        if (data !== undefined) {
                            if(data.success==true){
                                $scope.konsultasi = data.rec;
                                console.log($scope.konsultasi);
                            }
                        }
                    });
                }
                $scope.loadKonsultasi();

                $scope.Proses = function(){
                    $http({
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '<?php echo site_url( 'konsultasi/loadkonsultasi/' . $this->session->userdata('hashkey')) ; ?>',
                        method: "POST",
                        data: $.param({stop:0, limit :10000, tgl1 : $scope.tgl1, tgl2 : $scope.tgl2})
                    }).success(function(data) {
                        if (data !== undefined) {
                            if(data.success==true){
                                $scope.konsultasi = data.rec;
                                console.log($scope.konsultasi);
                            }
                        }
                    });
                }

            }
        </script>
        <div class="container detail" ng-app>        
            <div class="panel panel-success" ng-controller="KasusCtrl">
                <div class="panel-heading">
                    <h3 class="panel-title">Laporan Data User Telah Melakukan  Konsultasi</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-success">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                Tanggal :
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <input ng-model="tgl1" type="date" name="" id="input" class="form-control" value="" required="required" pattern="" title="">                                
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <input ng-model="tgl2" type="date" name="" id="input" class="form-control" value="" required="required" pattern="" title="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <button type="button" class="btn btn-primary" ng-click="Proses()">Proses</button>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-success">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nama</th>
                                        <th>Kelamin</th>
                                        <th>Umur</th>
                                        <th>Penyakit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="items in konsultasi">
                                        <td>{{items.k_id}}</td>
                                        <td>{{items.k_name}}</td>
                                        <td>{{items.k_kelamin}}</td>
                                        <td>{{items.k_umur}}</td>
                                        <td>
                                            <ul>
                                                <li ng-repeat="dtl in items.detail">
                                                {{dtl.p_name}} - {{dtl.kp_nilai}}
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- <a type="button" class="btn btn-danger" href="<?php echo site_url('konsultasi/hasil'); ?>">Selesai</a> -->
                </div>
            </div>

        </div> <!-- /container -->
        
    </body>
</html>
