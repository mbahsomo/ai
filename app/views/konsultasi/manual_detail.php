
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- The above 2 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Note there is no responsive meta tag here -->

        <link rel="icon" href="favicon.ico">

        <title>Hitung</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/lib/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/angular.min.js"></script>
    
    </head>

    <body>

        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- The mobile navbar-toggle button can be safely removed since you do not need it in a non-responsive implementation -->
                    <a class="navbar-brand" href="#">Gejala</a>
                </div>
                <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
                <div id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>            
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <style type="text/css">
            .detail{
                margin-top: 60px;
            }
        </style>
        <script type="text/javascript">
            function ManualCtrl($scope, $http){
                $scope.datasimpan = {
                    nama:'',
                    kelamin : 'L',
                    umur : 10
                };
                $scope.inc = 0;
                $scope.pertanyaan = [];
                $scope.pilih = 'F';

                $scope.maxtanya = 0;
                $scope.LoadMaxPertanyaan = function(){
                    $http({
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '<?php echo site_url( 'konsultasi/get_total/' . $this->session->userdata('hashkey')) ; ?>',
                        method: "POST"
                    }).success(function(data) {
                        if (data !== undefined) {
                            if(data.success==true){
                                $scope.maxtanya = data.total;
                            }
                        }
                    });
                }

                $scope.LoadMaxPertanyaan();

                $scope.LoadPertanyaan = function(){
                    $http({
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '<?php echo site_url( 'gejala/loadgejala/' . $this->session->userdata('hashkey')) ; ?>',
                        method: "POST",
                        data: $.param({stop:$scope.inc, limit :1})
                    }).success(function(data) {
                        if (data !== undefined) {
                            if(data.success==true){
                                $scope.pertanyaan = data.rec;
                                console.log($scope.pertanyaan);
                            }
                        }
                    });
                }
                $scope.LoadPertanyaan();

                $scope.SimpanPertanyaan = function(){
                    $scope.inc++;
                    if($scope.pilih==='T'){
                        $http({
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            url: '<?php echo site_url( 'konsultasi/simpan_detail/' . $this->session->userdata('hashkey')) ; ?>',
                            method: "POST",
                            data: $.param({g_id: $scope.pertanyaan[0]['g_id']})
                        }).success(function(data) {
                            if (data !== undefined) {
                                if(data.success==true){
                                    $scope.LoadMaxPertanyaan();
                                    $scope.pilih = 'F';
                                    if($scope.maxtanya>= <?php echo MAX_GEJALA; ?>){
                                        window.location.href = '<?php echo site_url('konsultasi/hasil'); ?>';
                                    }else{
                                        if(parseFloat($scope.inc) >= 5){
                                            window.location.href = '<?php echo site_url('konsultasi/manual_detail_table'); ?>';
                                        }else{
                                            $scope.LoadPertanyaan();    
                                        }
                                    }
                                    
                                }
                            }
                        });
                    }else{
                        if(parseFloat($scope.inc) >= 5){
                            window.location.href = '<?php echo site_url('konsultasi/manual_detail_table'); ?>';
                        }else{
                            $scope.LoadPertanyaan();    
                        }
                    }

                }
                
            }
        </script>
        <div class="container detail" ng-app>        
            <div class="panel panel-success" ng-controller="ManualCtrl">
                <div class="panel-heading">
                    <h3 class="panel-title">Gejala Yang di rasakan</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        <strong>Max Pemilihan Gejala = <?php echo MAX_GEJALA; ?></strong> Jumlah yang sudah dipilih {{maxtanya}}
                    </div>

                    <div class="alert alert-danger">
                        <strong>Pertanyaan</strong>
                        <p>[#{{pertanyaan[0]['g_id']}}] {{pertanyaan[0]['g_pertanyaan']}}</p></br>
                        <form class="form-horizontal" role="form" id="frmSimpan" >
                        <label>
                            <input type="radio" ng-model="pilih" value="T" name="pilih">
                            iya
                        </label><br/>
                        <label>
                            <input type="radio" ng-model="pilih" value="F" name="pilih">
                            Tidak
                        </label><br/>
                       </form>
                    </div>
                    <button type="button" class="btn btn-primary" ng-click="SimpanPertanyaan()">Lanjut</button>
                    <!-- <button type="button" class="btn btn-primary" ng-click="End()">Lebih banyak pertanyaan</button> -->
                    <a type="button" class="btn btn-danger" href="<?php echo site_url('konsultasi/manual_detail_table'); ?>" >Selesai</a>
                </div>
            </div>

        </div> <!-- /container -->
    
    </body>
</html>
