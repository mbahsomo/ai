
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- The above 2 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Note there is no responsive meta tag here -->

        <link rel="icon" href="favicon.ico">

        <title>About</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/lib/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/angular.min.js"></script>
    
    </head>

    <body>

        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- The mobile navbar-toggle button can be safely removed since you do not need it in a non-responsive implementation -->
                    <a class="navbar-brand" href="#">About</a>
                </div>
                <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
                <div id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>            
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <style type="text/css">
            .detail{
                margin-top: 60px;
            }
        </style>
        
        <div class="container detail" ng-app>        
            <div class="panel panel-success" ng-controller="ManualCtrl">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    
                    <div align="center">
                        <img class="cls-logo" src="<?php echo base_url('assets/images/depan.jpg');?>" class="img-responsive" alt="Image">
                    </div> 
                    <p align="justify">Website sistem pakar ini bertujuan untuk memberikan  kemudahan bagi masyarakat dalam mendiagnosa penyakit lambung dan usus yang  dideritanya. Adapun ciri-ciri utama dari gejala penyakit lambung  dan usus antara lain dapat berupa : mual, muntah, rasa sakit atau tidak nyaman  disekitar perut, dan permasalahan dalam buang air besar. </p>
                    <p align="justify">Untuk melakukan proses pendiagnosaan  penyakit caranya cukup mudah, yaitu pasien cukup menjawab pertanyaan-pertanyaan dari sistem berupa gejala  yang mungkin dirasakannya. Setelah itu sistem akan menampilkan hasil diagnosa penyakit pasien.<br />                    

                    



                </div>
            </div>

        </div> <!-- /container -->
    
    </body>
</html>
