<?php 
//print_r($rec);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- The above 2 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Note there is no responsive meta tag here -->

        <link rel="icon" href="favicon.ico">

        <title>Hitung</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/lib/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/report.css'); ?>" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/angular.min.js"></script>
        
        <style type="text/css">
            .detail{
                margin-top: 60px;
            }
        </style>
    </head>

    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- The mobile navbar-toggle button can be safely removed since you do not need it in a non-responsive implementation -->
                    <a class="navbar-brand" href="#">Gejala</a>
                </div>
                <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
                <div id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>            
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <style type="text/css">
            table.detail{
                width: 300px;
                margin-top: 0;
            }
        </style>
        <div class="container detail" >
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Hasil</h3>
                </div>
                <div class="panel-body" align="center">
                    <?php if (count($rec)>1) {?>
                    Gejala Terpilih : <br>
                    <?php
                    $data = array();
                    $data_tmp = array();
                    $data_penyakit = array();
                    $data_penyakit_tmp = array();                    
                    //print(set_penyakit($this->mdlrule->get_penyakit($rec[0]['g_id'])));
                    $m = array();
                    $t = array();
                    $loop = 0 ; 
                    foreach ($rec as $key => $value) {
                        echo '- ' . $value['g_name'] . '<br>';
                    }
                    echo "<br>";
                    foreach ($rec as $key => $value) {
                        /*echo "<pre>";
                        print_r ($data_penyakit);
                        echo "</pre>";*/
                        $data_tmp = $data;
                        $data = null; 
                        $data_penyakit_tmp = $data_penyakit;
                        $data_penyakit = null; 
                        if($loop==1){
                            //$m[0] = $rec[0]['g_kepercayaan'] ;
                            $m[] = array($rec[0]['g_kepercayaan'] ,'rule' => set_penyakit($this->mdlrule->get_penyakit($rec[0]['g_id'])) );
                            $t[0] = (1-$rec[0]['g_kepercayaan']);
                            //Ambil Rule
                            $m[] = array($rec[1]['g_kepercayaan'], 'rule' => set_penyakit($this->mdlrule->get_penyakit($rec[1]['g_id']))) ;
                            $t[1] = (1-$rec[1]['g_kepercayaan']);
                            echo 'm[0] =' . $m[0][0] . '<br>';
                            echo 't[0] =' . $t[0] . '<br>';
                            echo 'm[1] =' . $m[1][0] . '<br>';
                            echo 't[1] =' . $t[1] ;
                            echo "<table class='detail'>";
                                echo "<tr>";
                                    echo "<td>";
                                    echo "</td>";
                                    echo "<td>m [". $m[1]['rule']  .'] = '. $m[1][0] . "</td>";
                                    echo "<td>" . $t[1] . "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    $data[0][0] = $m[0][0] * $m[1][0];
                                    $data[0][1] = $m[0][0] * $t[1];

                                    $data_penyakit[0][0] = kali_penyakit($m[0]['rule'], $m[1]['rule']);
                                    $data_penyakit[0][1] = $m[1]['rule'];

                                    echo "<td>m [". $m[0]['rule']  .'] = ' . $m[0][0] . "</td>";
                                    //Simpan Penuakit
                                    echo "<td> [" . kali_penyakit($m[0]['rule'], $m[1]['rule']) .'] = ' . $data[0][0] .  "</td>";
                                    echo "<td>[" . $m[1]['rule']  .'] = ' . $data[0][1] . "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    $data[1][0] = $t[0] * $m[1][0];
                                    $data[1][1] = $t[0] * $t[1];

                                    $data_penyakit[1][0] = $m[0]['rule'];
                                    $data_penyakit[1][1] = '';

                                    echo "<td>"  . $t[0] . "</td>";
                                    echo "<td>[". $m[0]['rule'] . "] = " . $data[1][0] . "</td>";
                                    echo "<td>" . $data[1][1] . "</td>";
                                echo "</tr>";
                            echo "</table>";
                        }else if($loop>1){
                            echo "<br>Prsoes Ke " . $loop . "<br>";
                            $m[$loop] = array($value['g_kepercayaan'], 'rule' => set_penyakit($this->mdlrule->get_penyakit($value['g_id']))) ;
                            $t[$loop] = 1 - $value['g_kepercayaan'];
                            
                            echo "m[$loop] =" . $m[$loop][0] . '<br>';
                            echo "t[$loop] =" . $t[$loop] . '<br>';
                            echo "<table class='detail'>";
                                echo "<tr>";
                                    echo "<td></td>";
                                    echo "<td>m [ ". $m[$loop]['rule']  .'] = '. $m[$loop][0] . "</td>";
                                    echo "<td>" . $t[$loop] . "</td>";
                                echo "</tr>";
                                $i = 0; 
                                $kosong = $i;
                                for ($i=0; $i < count($data_tmp) ; $i++) {
                                    //if($data_penyakit_tmp[$i][0] !== ''){
                                        echo "<tr>";
                                            echo "<td> [" . $data_penyakit_tmp[$i][0] . "] = " . $data_tmp[$i][0] . "</td>";

                                            $data[$kosong][0] = $data_tmp[$i][0] * $m[$loop][0];
                                            $data_penyakit[$kosong][0] = kali_penyakit($data_penyakit_tmp[$i][0] , $m[$loop]['rule']) ;
                                            
                                            echo "<td>[" . $data_penyakit[$kosong][0] . '] = ' . $data_tmp[$i][0] * $m[$loop][0] . "</td>";

                                            $data[$kosong][1] = $data_tmp[$i][0] * $t[$loop];
                                            $data_penyakit[$kosong][1] =  $data_penyakit_tmp[$i][0]  ;
                                            echo "<td>[". $data_penyakit[$kosong][1] . "] = " . $data_tmp[$i][0] * $t[$loop] . "</td>";

                                        echo "</tr>";
                                        $kosong++;
                                    //}                                    
                                }
                                for ($j=0; $j < count($data_tmp) ; $j++) {
                                    //if($data_penyakit_tmp[$j][1] !== ''){
                                        echo "<tr>";

                                            echo "<td>[" . $data_penyakit_tmp[$j][1] . "] = " . $data_tmp[$j][1] . "</td>";

                                            $data[$kosong][0] = $data_tmp[$j][1] * $m[$loop][0];
                                            $data_penyakit[$kosong][0] = kali_penyakit($data_penyakit_tmp[$j][1], $m[$loop]['rule']);

                                            echo "<td>[" . $data_penyakit[$kosong][0] . "] =" . $data_tmp[$j][1] * $m[$loop][0] . "</td>";

                                            $data[$kosong][1] = $data_tmp[$j][1] * $t[$loop];
                                            $data_penyakit[$kosong][1] = $data_penyakit_tmp[$j][1];

                                            echo "<td>[" . $data_penyakit[$kosong][1] . '] = ' . $data_tmp[$j][1] * $t[$loop] . "</td>";
                                        echo "</tr>";
                                        $kosong++;
                                    //}
                                }

                            echo "</table>";
                        }
                        $loop++;
                    }                    
                    ?>
                    <pre>
                    <?php
                    $penyakit = hasil_penyakit($data_penyakit);
                    //hasil_penyakit($data_penyakit);
                    if($penyakit[0]!==''){
                        $kondisiw = explode(",", $penyakit);
                        echo "Terbesar Penyakit = ". $penyakit;
                        $this->mdl->set_null();
                        $this->mdl->set_table('penyakit');
                        $this->mdl->set_fields('*');
                        $this->mdl->set_where_in('p_id',$kondisiw);
                        $nmpenyakit = $this->mdl->get_data();
                    ?>
                    <table class="detail">
                        <thead>
                            <tr>
                                <th>Nama Penyakit</th>
                                <th>Nilai Probabilitas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $this->mdl->set_null();
                            $this->mdl->set_table('konsultasi_penyakit');
                            $this->mdl->set_params(array(
                                'k_id' => $this->session->userdata('nokonsultasi')
                            ));
                            $this->mdl->delete_data();
                            foreach ($nmpenyakit as $rows) {
                            ?>
                                <tr>
                                    <td><?php echo $rows['p_name']; ?></td>
                                    <td><?php 
                                        $ttl = 0;
                                        foreach ($data as $key => $value) {
                                            foreach ($data_penyakit[$key] as $key => $valuep) {
                                                if ($valuep === $penyakit){
                                                    $ttl += $value[$key]; 
                                                }
                                            }
                                        }
                                        $this->mdl->set_null();
                                        $this->mdl->set_table('konsultasi_penyakit');
                                        $this->mdl->set_fields(array(
                                            'k_id' => $this->session->userdata('nokonsultasi') ,
                                            'p_id' => $rows['p_id'],
                                            'kp_nilai' => $ttl,
                                            'date_entry' => date('Y-m-d H:i:s'),
                                            'date_edit' => date('Y-m-d H:i:s')
                                        ));
                                        $this->mdl->save_data();
                                        echo $ttl;
                                    ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php
                    }else{
                        echo "Penyakit tidak ditemukan ..";
                    }
                    ?>
                    </pre>
                    <?php }else{
                        echo "Pilih Lebih dari 1 Gejala untuk mendapatkan hasil penyakit";    
                    } ?>                
                </div>
            </div>

        </div> <!-- /container -->
        
    </body>
</html>