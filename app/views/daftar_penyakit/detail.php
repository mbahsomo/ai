<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- The above 2 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Note there is no responsive meta tag here -->

        <link rel="icon" href="favicon.ico">

        <title>Daftar</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/lib/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/angular.min.js"></script>
        
        <style type="text/css">
            .detail{
                margin-top: 60px;
            }
        </style>
    </head>

    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- The mobile navbar-toggle button can be safely removed since you do not need it in a non-responsive implementation -->
                    <a class="navbar-brand" href="#">Daftar Penyakit</a>
                </div>
                <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
                <div id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>            
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <script type="text/javascript">
            function KasusCtrl($scope, $http){
                $scope.LoadPenyakit = function(){
                    $scope.penyakit = [] ;
                    $http({
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '<?php echo site_url( 'daftar_penyakit/loadpenyakit/' . $this->session->userdata('hashkey')) ; ?>',
                        method: "POST",
                        data: $.param({stop:0, limit :1000})
                    }).success(function(data) {
                        if (data !== undefined) {
                            if(data.success==true){
                                $scope.penyakit = data.rec;
                                console.log($scope.penyakit);
                            }
                        }
                    });
                }
                $scope.LoadPenyakit();

                
            }
        </script>
        <div class="container detail" ng-app>        
            <div class="panel panel-success" ng-controller="KasusCtrl">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Penyakit</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-success">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nama Penyakit</th>
                                        <th>Keterangan</th>
                                        <th>Saran</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="items in penyakit">
                                        <td>{{items.p_id}}</td>
                                        <td>{{items.p_name}}</td>
                                        <td>{{items.p_keterangan}}</td>
                                        <td>{{items.p_saran}}</td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- <a type="button" class="btn btn-danger" href="<?php echo site_url('konsultasi/hasil'); ?>">Selesai</a> -->
                </div>
            </div>

        </div> <!-- /container -->
        
    </body>
</html>
