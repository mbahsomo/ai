```
-- MySQL Workbench Synchronization
-- Generated: 2015-08-28 19:29
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Sugik Puja Kusuma

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `db_spk`.`konsultasi_penyakit` (
  `kp_id` INT(11) NOT NULL AUTO_INCREMENT,
  `k_id` INT(11) NULL DEFAULT NULL,
  `p_id` INT(11) NULL DEFAULT NULL,
  `kp_nilai` FLOAT(11) NULL DEFAULT 0,
  PRIMARY KEY (`kp_id`),
  INDEX `fk_konsultasi_penyakit_konsultasi1_idx` (`k_id` ASC),
  INDEX `fk_konsultasi_penyakit_penyakit1_idx` (`p_id` ASC),
  CONSTRAINT `fk_konsultasi_penyakit_konsultasi1`
    FOREIGN KEY (`k_id`)
    REFERENCES `db_spk`.`konsultasi` (`k_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_konsultasi_penyakit_penyakit1`
    FOREIGN KEY (`p_id`)
    REFERENCES `db_spk`.`penyakit` (`p_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

```